/**
 * Helpers
 */
import { Markup } from 'telegraf'

/**
 * Delay method
 * @param {number} ms
 */
export function sleep(ms = 100) {
    if (typeof ms !== 'number') {
        throw Error('Invalid parameter: ms.')
    }

    return new Promise(resolve => setTimeout(resolve, ms))
}

/**
 * Parse value to boolean
 * @param {any} value
 */
export function toBoolean(value) {
    return value?.toString()?.toLowerCase?.() === 'true' ||
        Boolean(parseInt(value))
}

/**
 * Capitalize first letter of the passed string
 * @param {string} value
 */
export function firstCapital(value) {
    if (typeof value !== 'string') {
        throw Error('Invalid parameter: value.')
    }

    if (value === '') return value

    return value.charAt(0).toUpperCase() + value.slice(1)
}

/**
 * Number to words translator
 * @param {number} value - number
 * @param {array} words - array od declinations
 */
export function numberToWords(value, words) {
    const cases = [2, 0, 1, 1, 1, 2] // ru

    value = Math.abs(+value)

    if (typeof words === 'string') {
        words = words.split('_')
    }
    else if (! Array.isArray(words)) {
        throw Error('Invalid parameter: words.')
    }

    return words[(value % 100 > 4 && value % 100 < 20) ? 2 :
        cases[(value % 10 < 5) ? value % 10 : 5]]
}

/**
 * Get hidden text link
 * @param {string} url
 * @param {string} parseMode
 */
export function hiddenTextLink(url, parseMode = 'HTML') {
    if (! url) {
        throw Error('Invalid parameter: url.')
    }

    if (! parseMode) {
        throw Error('Invalid parameter: parseMode.')
    }

    switch (parseMode.toUpperCase()) {
        case 'MARKDOWN': return `[‎](${url})`
        case 'HTML': return `<a href="${url}">‎</a>`
        default: throw Error('Invalid parameter: parseMode.')
    }
}

/**
 * Validate instance
 * @param {object|number|string} value
 * @param {object|string} instance
 */
export function validateInst(value, instance) {
    if (! value || ! instance) return false

    if (typeof instance === 'function') {
        if (typeof value === 'number' || typeof value === 'string') {
            return instance.findByPk(value)
        }
        else {
            return value instanceof instance ? value : null
        }
    }
    else if (typeof instance === 'string') {
        instance = instance.split('|')

        if (typeof value === 'object') {
            for (let inst of instance) {
                if (value.constructor?.name === inst) {
                    return value
                }
            }
        }
    }

    return null
}

/**
 * Short for returning back to menu
 * @param {Context} ctx
 * @param {string} message
 * @param {array} data
 * @param {boolean} edit
 */
export async function backToModels(ctx, message, data, edit = true) {
    if (! message || typeof message !== 'string') {
        throw Error('Invalid parameter: message.')
    }
    if (! data || typeof data !== 'string') {
        throw Error('Invalid parameter: data.')
    }

    const args = [
        message,
        Markup.inlineKeyboard([
            [ Markup.button.callback('« Назад', `back-to-${data}`) ],
        ])
    ]

    if (! Boolean(edit)) return await ctx.reply(...args)

    return await ctx.editMessageText(...args)
}
