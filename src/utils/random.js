/**
 * Randomness
 */
import { randomBytes } from 'crypto'

const ENCODING = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
const ENCODING_LEN = ENCODING.length

/**
 * Secure random number
 */
function prng() {
    return randomBytes(1).readUint8() / 0x100
}

/**
 * Random
 * @param {Array|number|undefined} min
 * @param {number|undefined} max
 */
export function random(min, max) {
    if (typeof min === 'undefined') {
        return prng()
    }
    else if (typeof max === 'undefined') {
        if (min instanceof Array) {
            return min[Math.floor(prng() * min.length)]
        }
        else {
            return prng() * min
        }
    }
    else {
        if (min > max) {
            const temp = min
            min = max
            max = temp
        }

        return prng() * (max - min) + min
    }
}

/**
 * Random string
 * @param {number} length
 */
export function randomStr(length = 10) {
    if (!length || typeof length !== 'number') {
        throw Error('Invalid parameter: length.')
    }

    let _str = ''

    for (let l = 0; l < length; l++) {
        _str += ENCODING.charAt(Math.floor(prng() * ENCODING_LEN))
    }

    return _str
}

/**
 * Unique random string (simplified ulid/javascript)
 */
export function unique() {
    let encode = ''
    let time = Date.now()
    let timeMod = null

    while (time > 0) {
        timeMod = time % ENCODING_LEN

        encode = ENCODING.charAt(timeMod) + encode

        time = (time - timeMod) / ENCODING_LEN
    }

    return encode + randomStr(4)
}
