/**
 * Convert Telegram entities to HTML
 */

/**
 * Escape HTML symbols
 * @param {string} value
 * @returns
 */
export const escapeHTML = (() => {
    const _escapes = {
        '<': '&lt;',
        '>': '&gt;',
        '&': '&amp;',
        '"': '&quot;',
        '\'': '&#39;',
    }

    const _escapesRegExp = new RegExp(Object.keys(_escapes).join('|'), 'g')

    return text => {
        if (! text || typeof text !== 'string') return text

        return text.toString().replace(
            _escapesRegExp,
            elem => _escapes[elem] || elem
        )
    }
})()

/**
 * Remove tags
 * @param {string} text
 * @returns
 */
export function removeTags(text) {
    return text.replace(/(<([^>]+)>)/g, '')
}

/**
 * Wrap text to HTML tag
 * @param {string} text
 * @param {object} entity
 */
function wrapToTag(text, entity) {
    switch (entity?.type) {
        case 'bold': return `<b>${text}</b>`
        case 'italic': return `<i>${text}</i>`
        case 'underline': return `<u>${text}</u>`
        case 'strikethrough': return `<del>${text}</del>`
        case 'code': return `<code>${text}</code>`
        case 'pre':
            return entity.language ?
                `<pre><code class="language-${entity.language}">${text}</code></pre>` :
                `<pre>${text}</pre>`
        case 'spoiler': return `<span class="tg-spoiler">${text}</span>`
        case 'url': return `<a href="${removeTags(text)}">${text}</a>`
        case 'text_link': return `<a href="${entity.url}">${text}</a>`
        case 'text_mention': return `<a href="tg://user?id=${entity.user.id}">${text}</a>`
        default: return text
    }
}

/**
 * Wrap text to HTML based on entities
 * @param {string} text
 * @param {object} entities
 * @param {number} offset
 * @param {string} prevPart
 */
function wrapText(text, entities, offset = 0, prevPart = '') {
    if (entities.length === 0) {
        return prevPart + escapeHTML(text.slice(offset))
    }

    const entity = entities.pop()

    const part = text.slice(offset, entity.length)

    const wrap = wrapToTag(prevPart + escapeHTML(part), entity)

    return wrapText(text, entities, entity.length, wrap)
}

/**
 * Convert message to HTML text
 * @param {object|string} message
 */
export function toHTML(message) {
    if (typeof message === 'string') {
        return escapeHTML(message)
    }
    if (! message || typeof message !== 'object') {
        throw Error('Invalid parameter: message.')
    }
    if (! message.text) {
        return null
    }
    if (message.entities && ! Array.isArray(message.entities)) {
        throw Error('Invalid parameter: message.entities.')
    }
    if (! message.entities || message.entities.length === 0) {
        return escapeHTML(message.text)
    }

    let _offset = 0
    let _result = ''
    let { text, entities } = message

    const entityGroups = entities.reduce((red, elem) => {
        red[elem.offset] ??= []

        red[elem.offset].push(elem)

        return red
    }, {})

    const groupKeys = Object.keys(entityGroups)

    for (let groupKey of groupKeys) {
        const _entities = entityGroups[groupKey]
            .sort((a, b) => a.length + b.length)

        const _entity = _entities[0]

        /** Text before current entity */
        _result += escapeHTML(text.slice(_offset, _entity.offset))

        _offset = _entity.offset + _entity.length

        _result += wrapText(
            /** Part of the text based on max entity length */
            text.slice(
                _entity.offset,
                _entity.offset + _entity.length
            ),
            _entities
        )
    }

    /** Text after last entity */
    _result += escapeHTML(text.slice(_offset))

    return _result
}
