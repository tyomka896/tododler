/**
 * Download uploaded files
 */
import fs from 'fs'
import path from 'path'
import axios from 'axios'
import { unique } from '#utils/random.js'

import { validateInst } from './helpers.js'

/**
 * Download file from telegram's storage
 *
 * @param {Context} ctx
 * @param {string} fileId
 */
export async function download(ctx, fileId, options) {
    ctx = validateInst(ctx, 'Context')

    if (! ctx) throw Error('Invalid parameter: ctx.')
    if (! fileId) throw Error('Invalid parameter: fileId.')

    options = Object.assign({
        ext: null,
        folder: '',
    }, options)

    const fileName = unique() + (options.ext ? '.' + options.ext : '')

    const savePath = fs.existsSync(env('DOC_PATH')) ?
        path.join(env('DOC_PATH'), env('APP_NAME') || '', options.folder) :
        path.join(storagePath(), options.folder)

    if (! fs.existsSync(savePath)) {
        fs.mkdirSync(savePath, { recursive: true })
    }

    const saveAs = path.join(savePath, fileName)

    try {
        const { href } = await ctx.telegram.getFileLink(fileId)

        const { data } = await axios.get(href, { responseType: 'stream' })

        const stream = fs.createWriteStream(saveAs)

        data.pipe(stream)

        return new Promise(resolve => {
            stream.on('finish', () => {
                stream.close()
                resolve({ fileName, path: saveAs })
            })
        })
    } catch (error) {
        console.error(
            `Error download document with message - ${error.message}`
        )

        return null
    }
}

/**
 * Download document
 * @param {Context} ctx
 */
export async function downloadDocument(ctx) {
    ctx = validateInst(ctx, 'Context')

    if (! ctx) throw Error('Invalid parameter: ctx.')

    if (! ctx.message.document) return null

    const { document } = ctx.message
    const ext = document.file_name.split('.')

    const loadedFile = await download(
        ctx,
        document.file_id,
        {
            ext: ext.length > 1 ? ext.pop() : '',
            folder: 'documents',
        }
    )

    return {
        name: document.file_name,
        size: document.file_size,
        path: loadedFile.path,
    }
}

/**
 * Download image
 * @param {Context} ctx
 * @param {number} maxSize
 */
export async function downloadPhoto(ctx, options) {
    ctx = validateInst(ctx, 'Context')

    if (! ctx) throw Error('Invalid parameter: ctx.')

    if (! ctx.message.photo) return null

    options = Object.assign({
        maxSize: 1048576,
    }, options)

    /** File should be not more than max size specified */
    const photo = ctx.message.photo
        .reduce((prev, elem) =>
            elem.file_size < options.maxSize &&
            elem.file_size > prev.file_size ?
            elem : prev
        )
    
    const loadedFile = await download(
        ctx,
        photo.file_id,
        {
            ext: 'jpeg',
            folder: 'images',
        }
    )

    return {
        name: loadedFile.fileName,
        size: photo.file_size,
        path: loadedFile.path,
    }
}

/**
 * Download image
 * @param {Context} ctx
 * @param {number} maxSize
 */
export async function downloadSticker(ctx) {
    ctx = validateInst(ctx, 'Context')

    if (! ctx) throw Error('Invalid parameter: ctx.')

    if (! ctx.message.sticker) return null

    const { sticker } = ctx.message

    const loadedFile = await download(
        ctx,
        sticker.file_id,
        {
            ext: 'png',
            folder: 'stickers',
        }
    )

    return {
        name: loadedFile.fileName,
        size: sticker.file_size,
        path: loadedFile.path,
    }
}

/**
 * Download voice
 * @param {Context} ctx
 * @param {number} maxSize
 */
export async function downloadVoice(ctx) {
    ctx = validateInst(ctx, 'Context')

    if (! ctx) throw Error('Invalid parameter: ctx.')

    if (! ctx.message.voice) return null

    const { voice } = ctx.message

    const loadedFile = await download(
        ctx,
        voice.file_id,
        {
            ext: 'ogg',
            folder: 'voices',
        }
    )

    return {
        name: loadedFile.fileName,
        size: voice.file_size,
        path: loadedFile.path,
    }
}
