/**
 * Task execution
 */
import { col, Op } from 'sequelize'
import schedule from 'node-schedule'

import dayjs from '#utils/dayjs.js'
import { Task } from '#models/task.js'
import { executeTask } from '#controllers/taskController.js'

let _bot = null

/** Repeat queue tasks */
export const REPEAT_UNIT = '1 day'.split(' ')

async function taskExecution() {
    const untilDate = dayjs().add(...REPEAT_UNIT)

    const tasks = await Task.findAll({
        where: {
            execute_at: {
                [Op.and]: {
                    [Op.lte]: untilDate.toDate(),
                    [Op.gte]: col('Task.updated_at'),
                },
            },
            is_notified: 0,
            is_completed: 0,
        }
    })

    for (let task of tasks) {
        if (schedule.scheduledJobs[task.scheduleName()]) continue

        await executeTask(_bot, task)
    }

    const cron = untilDate.format('s m H D * *')

    schedule.scheduleJob('task-executor', cron, taskExecution)
}

export default async function(bot) {
    if (! bot) return null

    _bot = bot

    taskExecution()
}
