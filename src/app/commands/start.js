/**
 * Start the main scene requirement
 */
import { Markup } from 'telegraf'

import { Task } from '#models/task.js'
import { FriendInvite } from '#models/friend_invite.js'
import { getAuth } from '#controllers/userController.js'
import { shareTask } from '#controllers/taskController.js'
import { confirmInvite } from '#controllers/friendInviteController.js'

export default async function (ctx) {
    if (!(await getAuth(ctx)).utc) {
        return ctx.scene.enter('user', { act: 'utc' })
    }

    const [_, word] = ctx.message.text.split(' ')

    if (word) {
        const [key, value] = word.split('_')

        switch (key) {
            case 'invite':
                const invite = await FriendInvite.findByPk(value)

                if (invite) return confirmInvite(ctx, invite)

                break
            case 'task':
                const task = await Task.findByPk(value)

                if (task) return shareTask(ctx, task)

                break
        }
    }

    if (ctx.chat.type === 'private') {
        return ctx.scene.enter('user', { act: 'help' })
    }
    else {
        return ctx.reply(
            'Лучше напишите мне лично 👇\n',
            {
                ...Markup.inlineKeyboard([
                    Markup.button.url(
                        'Перейти в ToDodler',
                        `https://t.me/${env('BOT_NAME', 'BOT_NAME')}`,
                    ),
                ]),
                reply_to_message_id: ctx.message.message_id,
            }
        )
    }
}
