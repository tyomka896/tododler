/**
 * List of user's tasks
 */
export default async function (ctx) {
    if (ctx.chat.type !== 'private') return

    return ctx.scene.enter('user', { act: 'tasks' })
}
