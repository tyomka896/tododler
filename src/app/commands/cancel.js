/**
 * Cancel last operation
 */
import { isInput, onInput, isFile, onFile } from '#scene'
import { TaskFile } from '#models/task_file.js'
import { deleteTaskFile } from '#controllers/taskFileController.js'

export default async function(ctx) {
    if (ctx.chat.type !== 'private') return

    if (ctx.session._task?.fileId) {
        const file = await TaskFile.findByPk(ctx.session._task?.fileId)

        if (file) await deleteTaskFile(file)
    }

    if (! isInput(ctx) && ! isFile(ctx)) {
        return ctx.reply('Нет активной операции для отмены.')
    }

    onInput(ctx)
    onFile(ctx)

    return ctx.reply('Последняя операция была отменена.')
}
