/**
 * List of user's friends
 */
export default async function (ctx) {
    if (ctx.chat.type !== 'private') return

    return ctx.scene.enter('user', { act: 'friends' })
}
