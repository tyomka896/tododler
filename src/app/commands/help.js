/**
 * Help command
 */
import { getAuth } from '#controllers/userController.js'

export default async function (ctx) {
    if (ctx.chat.type !== 'private') return

    const auth = await getAuth(ctx)

    const rows = [
        '/tasks - все задачи\n',
        '/friends - друзья\n\n',

        '<b>Основные команды</b>\n',
        '/newtask - создать задачу\n',
        // '/newfndtask - создать задачу для друга\n',
        '/addfnd - добавить друга\n',

        '\n<b>Дополнительно</b>\n',
        '/settings - настройки\n',
        '/cancel - отмена текущей операции',
    ]

    if (auth.admin()) {
        rows.push('\n\n<b>Админ</b>\n')
        rows.push('<code>/role</code> - изменить роль\n')
        rows.push('<code>/task</code> - информация о задаче\n')
        rows.push('/dump - снять дам БД\n')
    }

    return ctx.reply(rows.join(''), { parse_mode: 'HTML' })
}
