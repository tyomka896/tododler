/**
 * Create task for friend command
 */
export default async function(ctx) {
    if (ctx.chat.type !== 'private') return

    return ctx.scene.enter('user', { act: 'newfndtask' } )
}
