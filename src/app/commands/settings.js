/**
 * Setting command
 */
export default async function(ctx, next) {
    if (ctx.chat.type !== 'private') return

    return ctx.scene.enter('user', { act: 'settings' } )
}
