/**
 * Find specific task
 */
import { sleep } from '#utils/helpers.js'
import { Task } from '#models/task.js'
import { getAuth } from '#controllers/userController.js'

export default async function(ctx, next) {
    if (ctx.chat.type !== 'private') return

    const auth = await getAuth(ctx)

    if (! auth.admin()) return next()

    let taskIds = ctx.message.text.split(' ').splice(1)

    if (! taskIds) return next()

    taskIds = taskIds.slice(0, 5)

    let task, fromUser, toUser, file, _message

    for (let taskId of taskIds) {
        task = await Task.findByPk(taskId)

        if (! task) {
            await ctx.reply(`Задача #${taskId} не найдена.`)
        }

        fromUser = await task.getAuthor()
        toUser = await task.getTarget()

        _message = ''

        if (task.file_id) {
            file = await task.getFile()

            _message += file ? file.link : ''
        }

        _message += `<u>${task.id}</u>\n\n` +
            `Задача от <b>${task.executionDate(auth.utc)}</b>\n` +
            `Между <b>${fromUser.fullName(false)}</b> → ` +
            `<b>${toUser.fullName(false)}</b>\n\n` +
            `${task.text}\n\n`

        if (task.details) {
            _message += `<b>P.S.</b> <i>${task.details}</i>\n\n`
        }

        await ctx.reply(
            _message,
            {
                parse_mode: 'HTML',
                disable_web_page_preview: ! task.file_id,
            }
        )

        await sleep(500)
    }
}
