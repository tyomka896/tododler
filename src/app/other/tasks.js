/**
 * Task handler
 */
import { Markup } from 'telegraf'

import { unique } from '#utils/random.js'
import { toHTML } from '#utils/entities.js'
import { Task } from '#models/task.js'
import { scheduleTask } from '#controllers/taskController.js'
import { getAuth } from '#controllers/userController.js'

async function taskComplete(ctx) {
    const model = await Task.findByPk(ctx.match[1])

    if (!model) return

    await model.update({ is_completed: 1 })
    console.log(`Task #${model.id} fast completed.`)

    try {
        await ctx.deleteMessage()
    } catch {
        const _message = toHTML(ctx.update.callback_query.message)

        await ctx.editMessageText(
            _message,
            {
                parse_mode: 'HTML',
                disable_web_page_preview: !model.file_id,
            }
        )
    }
}

async function addTask(ctx) {
    const task = await Task.findByPk(ctx.match[1])

    if (!task) return

    const auth = await getAuth(ctx)

    const { text, details, is_notified, file_id, execute_at } = task

    const newTask = await Task.create({
        id: unique(),
        author_id: auth.id,
        text,
        details,
        target_id: auth.id,
        is_notified,
        file_id,
        execute_at,
    })
    console.log(`Task #${task.id} copied as #${newTask.id} by user #${auth.id}.`)

    await scheduleTask(ctx, newTask)

    return ctx.editMessageText(
        'Задача успешно скопирована в ваш список.',
        Markup.inlineKeyboard([
            [Markup.button.callback('К задаче »', `task-model-${newTask.id}`)]
        ]),
    )
}

/**
 * Init tasks handlers
 * @param {Telegraf} bot
 */
export default function (bot) {
    bot.action(/task-fast-complete-(\w{10,})/, taskComplete)
    bot.action(/task-fast-add-(\w{10,})/, addTask)
}