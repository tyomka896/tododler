/**
 * Friend handler
 */
import { User } from '#models/user.js'
import { getAuth } from '#controllers/userController.js'
import { declineFriend, acceptFriend } from '#controllers/friendController.js'

/**
 * Decline friend request
 */
async function friendDecline(ctx) {
    const fromUser = await User.findByPk(ctx.match[1])

    if (! fromUser) return

    const toUser = await getAuth(ctx)

    await declineFriend(ctx, fromUser, toUser)

    await ctx.telegram.sendMessage(
        fromUser.id,
        'Ваш запрос дружбы отклонен пользователем ' +
        `<b>${toUser.fullName()}</b>.`,
        { parse_mode: 'HTML' }
    )

    return ctx.editMessageText(
        'Запрос дружбы отклонен для пользователя ' +
        `<b>${fromUser.fullName()}</b>.`,
        { parse_mode: 'HTML' }
    )
}

/**
 * Accept friend request
 */
async function friendAccept(ctx) {
    const fromUser = await User.findByPk(ctx.match[1])

    if (! fromUser) return

    const toUser = await getAuth(ctx)

    await acceptFriend(ctx, fromUser, toUser)

    await ctx.telegram.sendMessage(
        fromUser.id,
        `Пользователь <b>${toUser.fullName()}</b> ` +
        'принял ваш запрос дружбы.',
        { parse_mode: 'HTML' }
    )

    return ctx.editMessageText(
        'Запрос дружбы от пользователя ' +
        `<b>${fromUser.fullName()}</b> принят.`,
        { parse_mode: 'HTML' }
    )
}

/**
 * Init tasks handlers
 * @param {Telegraf} bot
 */
export default function(bot) {
    bot.action(/addfnd-fast-decline-(\d*)/, friendDecline)
    bot.action(/addfnd-fast-accept-(\d*)/, friendAccept)
}