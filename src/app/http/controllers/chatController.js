/**
 * Chat controller
 */
import { Chat } from '#models/chat.js'

/** Local storage for quick access */
const cache = {}

export async function getChat(ctx) {
    if (ctx.constructor?.name !== 'Context') {
        throw Error('Invalid parameter: ctx.')
    }

    const model = cache[ctx.chat.id] ||
        await Chat.findByPk(ctx.chat.id)

    if (model && ! cache[ctx.chat.id]) {
        cache[model.id] = model
    }

    return model
}

export async function createChat(data) {
    if (typeof data !== 'object') {
        throw Error('Invalid parameter: data.')
    }

    const model = await Chat.create({
        id: data.id.toString(),
        name: data.name,
    })

    cache[model.id] = model

    console.log(`New chat #${model.id} '${model.name}' was created!`)

    return model
}
