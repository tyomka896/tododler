/**
 * Friend invite controller
 */
import { Markup } from 'telegraf'

import { unique } from '#utils/random.js'
import { validateInst } from '#utils/helpers.js'
import { Friend } from '#models/friend.js'
import { FRIEND_TYPE } from '#models/friend_type.js'
import { FriendInvite } from '#models/friend_invite.js'
import { getAuth } from '#controllers/userController.js'
import { friendsStatus } from '#controllers/friendController.js'

/**
 * Create or find existing one token
 * @param {Telegraf|Context} bot
 */
export async function getInviteToken(bot) {
    bot = await validateInst(bot, 'Telegraf|Context')

    if (! bot) throw Error('Invalid parameter: bot.')

    const auth = await getAuth(bot)

    let invite = await FriendInvite.findOne({
        where: {
            from_id: auth.id,
            to_id: null,
        }
    })

    if (! invite) {
        invite = await FriendInvite.create({
            id: unique(),
            from_id: auth.id,
        })

        console.log(
            `New invite token #${invite.id} ` +
            `by user #${auth.id} created.`
        )
    }

    return invite
}

/**
 * Create invite share link
 * @param {FriendInvite} invite
 */
export async function tokenShareLink(invite) {
    invite = await validateInst(invite, FriendInvite)

    if (! invite) throw Error('Invalid parameter: invite.')

    const fromUser = await invite.getFromUser()

    const botLink =
        `https://t.me/${env('BOT_NAME', 'BOT_NAME')}?` +
        `start=invite_${invite.id}`

    const shareText = `\nЗапрос в друзья от ${fromUser.fullName()}.`

    return 'https://t.me/share/url?' +
        `url=${encodeURI(botLink)}&` +
        `text=${encodeURI(shareText)}`
}

/**
 * Confirm friend invitation
 * @param {Telegraf|Context} bot
 * @param {FriendInvite} invite
 */
export async function confirmInvite(ctx, invite) {
    ctx = await validateInst(ctx, 'Context')
    invite = await validateInst(invite, FriendInvite)

    if (! ctx) throw Error('Invalid parameter: ctx.')
    if (! invite) throw Error('Invalid parameter: invite.')

    if (invite.to_id) {
        return ctx.reply(
            'Приглашение уже устарело.\n' +
            'Запросите другую ссылку для добавления в друзья.'
        )
    }

    const toUser = await getAuth(ctx)
    const fromUser = await invite.getFromUser()

    if (toUser.id === fromUser.id) {
        return ctx.reply(
            'Дружить с собой нужно по умолчанию.\n' +
            'Отправьте ссылку другому человеку, кроме себя.'
        )
    }

    let _message
    const { status, initiator } = await friendsStatus(fromUser, toUser)

    switch (status) {
        case FRIEND_TYPE.ACCEPT:
            _message = 'Пользователь уже у вас в друзьях.'
            break
        case FRIEND_TYPE.BLOCK:
            _message = toUser.id === initiator.id ?
                'Вы заблокировали этого пользователя.' :
                'Пользователь ваc заблокировал.'
            break
    }

    if (_message) return ctx.reply(_message)

    await invite.update({ to_id: toUser.id })

    await ctx.reply(
        `Принять запрос дружбы от <b>${fromUser.fullName()}</b>?`,
        {
            ...Markup.inlineKeyboard([
                [
                    Markup.button.callback(
                        'Нет',
                        `addfnd-fast-decline-${fromUser.id}`
                    ),
                    Markup.button.callback(
                        'Принять',
                        `addfnd-fast-accept-${fromUser.id}`
                    ),
                ]
            ]),
            parse_mode: 'HTML',
        }
    )

    const friend = await Friend.findOne({
        where: { from_id: fromUser.id, to_id: toUser.id }
    })

    if (friend) {
        await friend.update({ type_id: FRIEND_TYPE.REQUEST })
    }
    else {
        await Friend.create({
            from_id: fromUser.id,
            to_id: toUser.id,
            type_id: FRIEND_TYPE.REQUEST,
        })
    }

    console.log(
        `Confirm friend invite #${invite.id} sent between ` +
        `#${fromUser.id} → #${toUser.id}.`
    )

    return true
}
