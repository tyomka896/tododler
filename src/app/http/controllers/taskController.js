/**
 * Task controller
 */
import fs from 'fs'
import { Markup } from 'telegraf'
import schedule from 'node-schedule'

import dayjs from '#utils/dayjs.js'
import { unique } from '#utils/random.js'
import { validateInst } from '#utils/helpers.js'
import { Task } from '#models/task.js'
import { TaskFile } from '#models/task_file.js'
import { FRIEND_TYPE } from '#models/friend_type.js'
import { getAuth } from '#controllers/userController.js'
import { deleteTaskFile } from '#controllers/taskFileController.js'
import { REPEAT_UNIT } from '#app/schedule/tasks.js'

/**
 * Create task
 * @param {object} data
 */
export async function createTask(bot, data) {
    bot = await validateInst(bot, 'Telegraf|Context')

    if (! bot) throw Error('Invalid parameter: bot.')

    if (typeof data !== 'object') {
        throw Error('Invalid parameter: data.')
    }

    const model = await Task.create({
        id: unique(),
        ...data,
    })

    console.log(
        `New task #${model.id} created between ` +
        `#${data.author_id} → #${data.target_id}.`
    )

    await scheduleTask(bot, model)

    return model
}

/**
 * Schedule task execution if needed
 * @param {Telegraf|Context} bot
 * @param {Task|string} task
 */
export async function scheduleTask(bot, task) {
    bot = await validateInst(bot, 'Telegraf|Context')
    task = await validateInst(task, Task)

    if (! bot) throw Error('Invalid parameter: bot.')
    if (! task) throw Error('Invalid parameter: task.')

    schedule.cancelJob(task.scheduleName())

    if (! task.isExpired()) {
        const [ unit, period ] = REPEAT_UNIT

        if (dayjs(task.execute_at).diff(dayjs(), period) < unit) {
            await executeTask(bot, task)
        }
    }

    return true
}

/**
 * Notify chat about task
 * @param {Telegraf|Context} bot
 * @param {Task|string} task
 */
export async function notifyTask(bot, task) {
    bot = await validateInst(bot, 'Telegraf|Context')
    task = await validateInst(task, Task)

    if (! bot) throw Error('Invalid parameter: bot.')
    if (! task) throw Error('Invalid parameter: task.')

    const replyMarkup = ! +task.is_private ? {} : {
        ...Markup.inlineKeyboard([
            [
                Markup.button.callback(
                    'Завершить',
                    `task-fast-complete-${task.id}`
                ),
                Markup.button.callback(
                    'Перенести »',
                    `task-edit-execute-${task.id}`
                ),
            ]
        ])
    }

    try {
        let _message = ''

        const file = await task.getFile()

        const textAndDetails = task.textAndDetails()

        _message += file ? file.link : ''

        if (textAndDetails) {
            _message += textAndDetails
        }
        else if (file) {
            if (file.isVoice()) {
                _message += '<i>Голосовое сообщение</i>'
            }
            else if (file.isImage()) {
                _message += '<i>Изображение</i>'
            }
        }

        await bot.telegram.sendMessage(
            task.target_id,
            _message,
            {
                ...replyMarkup,
                parse_mode: 'HTML',
                disable_web_page_preview: ! file,
            }
        )

        await task.update({ is_notified: 1 })
        console.log(`Task #${task.id} notified.`)
    } catch (error) {
        console.error(
            `Error to send task #${task.id} ` +
            `to chat #${task.target_id} - ` +
            error.message
        )

        await task.update({ is_completed: 1 })

        return false
    }
    finally {
        schedule.cancelJob(task.scheduleName())
    }

    return true
}

/**
 * Execute task if necessary
 * @param {Telegraf|Context} bot
 * @param {Task|string} task
 */
export async function executeTask(bot, task) {
    bot = await validateInst(bot, 'Telegraf|Context')
    task = await validateInst(task, Task)

    if (! bot) throw Error('Invalid parameter: bot.')
    if (! task) throw Error('Invalid parameter: task.')

    if (task.is_notified || task.is_completed) return false

    if (task.isExpired()) {
        console.log(`Expired task #${task.id} discovered.`)
        return await notifyTask(bot, task)
    }

    const cron = dayjs(task.execute_at).format('s m H D * *')

    schedule.scheduleJob(
        task.scheduleName(),
        cron,
        async () => await notifyTask(bot, task.id)
    )

    return true
}

/**
 *
 * @param {Context} ctx
 * @param {Task|string} task
 */
export async function shareTask(ctx, task) {
    ctx = await validateInst(ctx, 'Context')
    task = await validateInst(task, Task)

    if (! ctx) throw Error('Invalid parameter: ctx.')
    if (! task) throw Error('Invalid parameter: task.')

    let _message = '',
        replyMarkup = {}

    const file = await task.getFile()

    const toUser = await getAuth(ctx)

    const fromUser = await task.getAuthor()

    const textAndDetails = task.textAndDetails()

    _message += file ? file.link : ''

    _message += `Задача от <b>${task.executionDate(toUser.utc)}</b>\n` +
        `Автор <b>${fromUser.fullName(false)}</b>\n\n`

    if (textAndDetails) {
        _message += `${textAndDetails}\n\n`
    }

    if (task.author_id === toUser.id) {
        _message += '<i>❗️Вы автор задачи, она уже есть в вашем в списке</i>.'
    }
    else if (task.target_id === toUser.id) {
        _message += '<i>❗️Эта задача создана для вас</i>.'
    }
    else {
        _message += '<i>❓Скопировать задачу</i>'

        replyMarkup = {
            ...Markup.inlineKeyboard([
                [
                    Markup.button.callback(
                        'Нет',
                        'delete-message'
                    ),
                    Markup.button.callback(
                        'Добавить',
                        `task-fast-add-${task.id}`
                    ),
                ]
            ])
        }
    }

    console.log(
        `Task #${task.id} shared between ` +
        `#${fromUser.id} → #${toUser.id}.`
    )

    return ctx.reply(
        _message,
        {
            ...replyMarkup,
            parse_mode: 'HTML',
            disable_web_page_preview: ! file,
        }
    )
}

/**
 * Update task file
 * @param {Task|string} task
 * @param {object} info
 */
export async function updateTaskFile(task, info) {
    task = await validateInst(task, Task)

    if (! task) throw Error('Invalid parameter: task.')

    if (! info.name || ! info.path || ! fs.existsSync(info.path)) {
        throw Error('Invalid parameter: info.')
    }

    await emptyTaskFile(task)

    const file = await TaskFile.create({
        name: info.name,
        size: info.size || 0,
        path: info.path.replace(/\\/g, '/'),
        type_id: info.type || FRIEND_TYPE.IMAGE,
    })

    await task.update({ file_id: file.id })
    console.log(
        `File #${file.id}:${file.type_id} for task #${task.id} updated.`
    )

    return true
}

/**
 * Empty task image
 * @param {Task|string} task
 */
export async function emptyTaskFile(task) {
    task = await validateInst(task, Task)

    if (! task) throw Error('Invalid parameter: task.')

    const file = await task.getFile()

    if (file) {
        const tasksCount = await file.countTasks()

        if (tasksCount > 1) {
            await task.update({ file_id: null })

            console.log(
                `File #${file.id}:${file.type_id} ` +
                `for task #${task.id} cleared.`
            )
        }
        else {
            await deleteTaskFile(file)

            console.log(
                `File #${file.id}:${file.type_id} ` +
                `for task #${task.id} deleted.`
            )
        }
    }

    return true
}

/**
 * Complete task
 * @param {Task|string} task
 */
export async function completeTask(task) {
    task = await validateInst(task, Task)

    if (! task) throw Error('Invalid parameter: task.')

    if (task.is_completed) return false

    await task.update({ is_completed: 1 })
    console.log(`Task #${task.id} completed manually.`)

    schedule.cancelJob(task.scheduleName())

    return true
}

/**
 * Delete task
 * @param {Task|string} task
 */
export async function deleteTask(task) {
    task = await validateInst(task, Task)

    if (! task) throw Error('Invalid parameter: task.')

    await emptyTaskFile(task)

    await task.destroy()
    console.log(`Task #${task.id} deleted.`)

    schedule.cancelJob(task.scheduleName())

    return true
}
