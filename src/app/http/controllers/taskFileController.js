/**
 * Task file controller
 */
import fs from 'fs'

import { TaskFile } from '#models/task_file.js'
import { validateInst } from '#utils/helpers.js'

/**
 * Delete file
 * @param {TaskFile|string} taskFile
 */
export async function deleteTaskFile(taskFile) {
    taskFile = await validateInst(taskFile, TaskFile)

    if (! taskFile) throw Error('Invalid parameter: taskFile.')

    if (fs.existsSync(taskFile.path)) {
        fs.unlinkSync(taskFile.path)
    }

    await taskFile.destroy()

    return true
}
