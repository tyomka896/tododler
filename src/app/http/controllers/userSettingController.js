/**
 * User setting controller
 */
import { UserSetting } from '#models/user_setting.js'

/**
 * Get user setting value
 * @param {User|number} user
 * @param {string} type_id
 * @param {any} def
 */
export async function getUserSettingValue(user, type_id, def = null) {
    if (user?.constructor?.name === 'User') {
        user = user.id
    }
    if (typeof user !== 'number' && typeof user !== 'string') {
        throw Error('Invalid parameter: user.')
    }

    if (typeof type_id !== 'string') {
        throw Error('Invalid parameter: type_id.')
    }

    const model = await UserSetting.findOne({
        where: { user_id: user, type_id },
    })

    return model ? model.value : def
}
