/**
 * Friend controller
 */
import { Op } from 'sequelize'

import { validateInst } from '#utils/helpers.js'
import { User } from '#models/user.js'
import { Friend } from '#models/friend.js'
import { FRIEND_TYPE } from '#models/friend_type.js'

/**
 * Friends status
 * @param {User|number} fromUser
 * @param {User|number} toUser
 */
export async function friendsStatus(fromUser, toUser) {
    fromUser = await validateInst(fromUser, User)
    toUser = await validateInst(toUser, User)

    if (! fromUser) throw Error('Invalid parameter: fromUser.')
    if (! toUser) throw Error('Invalid parameter: toUser.')

    const models = await Friend.findAll({
        where: {
            [Op.or]: [
                { from_id: fromUser.id, to_id: toUser.id },
                { from_id: toUser.id, to_id: fromUser.id },
            ],
        },
        include: [ 'fromUser', 'toUser' ],
    })

    const result = { status: false }

    if (models.length === 1) {
        const _middle = models[0]

        result.status = _middle.type_id
        result.initiator = _middle.fromUser
    }
    else if (models.length === 2) {
        const [ _left, _right ] = models

        if (
            _left.type_id === _right.type_id &&
            _left.type_id === FRIEND_TYPE.ACCEPT
        ) {
            result.status = FRIEND_TYPE.ACCEPT
        }
        else if (_left.type_id === FRIEND_TYPE.BLOCK) {
            result.status = FRIEND_TYPE.BLOCK
            result.initiator = _left.fromUser
        }
        else if (_right.type_id === FRIEND_TYPE.BLOCK) {
            result.status = FRIEND_TYPE.BLOCK
            result.initiator = _right.fromUser
        }
    }

    return result
}

/**
 * Decline friend request
 * @param {Telegraf|Context} bot
 * @param {User|number} fromUser
 * @param {User|number} toUser
 */
export async function declineFriend(bot, fromUser, toUser) {
    bot = await validateInst(bot, 'Telegraf|Context')
    fromUser = await validateInst(fromUser, User)
    toUser = await validateInst(toUser, User)

    if (! bot) throw Error('Invalid parameter: bot.')
    if (! fromUser) throw Error('Invalid parameter: fromUser.')
    if (! toUser) throw Error('Invalid parameter: toUser.')

    const friend = await Friend.findOne({
        where: { from_id: fromUser.id, to_id: toUser.id }
    })

    await friend.update({ type_id: FRIEND_TYPE.REJECT })

    console.log(
        `Friend request from user #${fromUser.id} ` +
        `to #${toUser.id} declined.`
    )

    return true
}

/**
 * Accept friend request
 * @param {Telegraf|Context} bot
 * @param {User|number} fromUser
 * @param {User|number} toUser
 */
export async function acceptFriend(bot, fromUser, toUser) {
    bot = await validateInst(bot, 'Telegraf|Context')
    fromUser = await validateInst(fromUser, User)
    toUser = await validateInst(toUser, User)

    if (! bot) throw Error('Invalid parameter: bot.')
    if (! fromUser) throw Error('Invalid parameter: fromUser.')
    if (! toUser) throw Error('Invalid parameter: toUser.')

    const friendFrom = await Friend.findOne({
        where: {
            from_id: fromUser.id,
            to_id: toUser.id,
        }
    })

    await friendFrom.update({ type_id: FRIEND_TYPE.ACCEPT })

    const friendTo = await Friend.findOne({
        where: { from_id: toUser.id, to_id: fromUser.id }
    })

    if (friendTo) {
        await friendTo.update({ type_id: FRIEND_TYPE.ACCEPT })
    }
    else {
        await Friend.create({
            from_id: toUser.id,
            to_id: fromUser.id,
            type_id: FRIEND_TYPE.ACCEPT,
        })
    }

    console.log(
        `Friend request from user #${fromUser.id} ` +
        `to #${toUser.id} accepted.`
    )

    return true
}
