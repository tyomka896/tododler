/**
 * Setting sequelize connection
 */
import { Sequelize } from 'sequelize'

function logging(query) {
    console.log(
        new Date().toISOString().replace(/T/, ' ') + ': ' +
        query.replace('Executing (default): ', '')
    )
}

/**
 * Database connection
 */
export const sequelize = new Sequelize({
    dialect: 'postgres',
    host: env('DB_HOST', 'localhost'),
    port: env('DB_PORT', '5432'),
    database: env('DB_DATABASE', 'postgres'),
    username: env('DB_USERNAME', 'postgres'),
    password: env('DB_PASSWORD', 'postgres'),
    logging: env('DB_DEBUG', false) ? logging : false,
    logQueryParameters: env('DB_DEBUG', false),
    timezone: '+00:00',
})
