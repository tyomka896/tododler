/**
 * User model
 */
import { Model, DataTypes } from 'sequelize'
import { sequelize } from '#connection'
import { Friend } from './friend.js'
import { Session } from './session.js'
import { UserSetting } from './user_setting.js'
import { FriendInvite } from './friend_invite.js'

/**
 * Extending model
 */
export class User extends Model {
    /**
     * If the user's status admin
     */
    admin() {
        return this.role === 'admin'
    }

    /**
     * Link name from login or first and last names
     */
    linkName() {
        if (this.username) return `@${this.username}`

        return [
            this.last_name || '',
            this.first_name
        ]
            .filter(elem => elem)
            .join(' ')
    }

    /**
     * Concat last name, first name and login
     * @param {boolean} login
     */
    fullName(login = true) {
        return [
            this.last_name || '',
            this.first_name,
            this.username && login ? `@${this.username}` : ''
        ]
            .filter(elem => elem)
            .join(' ')
    }

    /**
     * Get user setting
     * @param {string} key
     */
    async getItem(key) {
        if (! key) {
            throw Error('Invalid parameter: key.')
        }

        return await UserSetting.findOne({
            where: {
                user_id: this.id,
                type_id: key,
            },
        })
    }

    /**
     * Get user setting value
     * @param {string} key
     * @param {any} def
     */
    async getItemValue(key, def = null) {
        const item = await this.getItem(key)

        return item ? item.value : def
    }

    /**
     * Set user setting
     * @param {string} key
     * @param {any} value
     */
     async setItem(key, value) {
        if (! key) {
            throw Error('Invalid parameter: key.')
        }
        if (value === null || value === undefined) {
            throw Error('Invalid parameter: value.')
        }

        if (typeof value === 'boolean' || typeof value === 'number') {
            value = (+value).toString()
        }
        else if (typeof value === 'object' || Array.isArray(value)) {
            value = JSON.stringify(value)
        }

        const keyPair = { user_id: this.id, type_id: key }

        const setting = await UserSetting.findOne({ where: keyPair })

        return setting ? await setting.update({ value }) :
            await UserSetting.create({ ...keyPair, value })
    }
}

/**
 * Model structure
 */
User.init({
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
    },
    utc: {
        type: DataTypes.STRING(6),
    },
    role: {
        type: DataTypes.STRING(10),
        allowNull: false,
        defaultValue: 'user',
    },
    username: {
        type: DataTypes.STRING(50),
    },
    first_name: {
        type: DataTypes.STRING(100),
        allowNull: false,
    },
    last_name: {
        type: DataTypes.STRING(100),
    },
    created_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    updated_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    last_visit: {
        type: DataTypes.DATE,
        allowNull: false,
    }
}, {
    sequelize,
    tableName: 'user',
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
})

/**
 * Get the sessions for the user.
 */
User.hasMany(Session, {
    foreignKey: 'user_id',
    as: 'sessions'
})

/**
 * Get the user that owns the setting.
 */
UserSetting.belongsTo(User, {
    foreignKey: 'user_id',
    as: 'user',
})

/**
 * Get the settings for the user.
 */
User.hasMany(UserSetting, {
    foreignKey: 'user_id',
    as: 'setting',
})

/**
 * Get the user_from that owns the friend.
 */
Friend.belongsTo(User, {
    foreignKey: 'from_id',
    as: 'fromUser',
})

/**
 * Get the user_to that owns the friend.
 */
Friend.belongsTo(User, {
    foreignKey: 'to_id',
    as: 'toUser',
})

/**
 * Get the friends for the user.
 */
User.hasMany(Friend, {
    foreignKey: 'from_id',
    as: 'friends',
})

/**
 * Get the user_from that owns the friend invite.
 */
FriendInvite.belongsTo(User, {
    foreignKey: 'from_id',
    as: 'fromUser',
})

/**
 * Get the user_to that owns the friend invite.
 */
FriendInvite.belongsTo(User, {
    foreignKey: 'to_id',
    as: 'toUser',
})
