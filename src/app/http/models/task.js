/**
 * Task model
 */
import { Model, DataTypes } from 'sequelize'

import { sequelize } from '#connection'
import dayjs from '#utils/dayjs.js'
import { firstCapital } from '#utils/helpers.js'
import { escapeHTML, toHTML } from '#utils/entities.js'
import { User } from './user.js'
import { TaskFile } from './task_file.js'

/**
 * Extending model
 */
export class Task extends Model {
    /**
     * Schedule name
     */
    scheduleName() {
        return `task-execution-${this.id}`
    }

    /**
     * If task expired
     */
    isExpired() {
        return dayjs().diff(this.execute_at) >= 0
    }

    /**
     * Execution formatted date
     * @param {string} utc
     * @param {string} format
     */
    executionDate(utc, format = 'dd, D MMMM, YYYY года, HH:mm') {
        if (!utc || ! /^[+-]{1}\d{2}:\d{2}/.test(utc)) {
            throw Error('Invalid parameter: utc.')
        }

        return firstCapital(
            dayjs(this.execute_at).utcOffset(utc)
                .format(format)
        )
    }

    /**
     * Text and details together
     */
    textAndDetails() {
        return [
            this.text ? this.text : '',
            this.details ? `<b>P.S.</b> <i>${this.details}</i>` : ''
        ]
            .filter(elem => !!elem)
            .join('\n\n')
    }

    /**
     * Create share link
     */
    shareLink(user) {
        if (!(user instanceof User)) throw Error('Invalid parameter: user.')

        const botLink =
            `https://t.me/${env('BOT_NAME', 'BOT_NAME')}?` +
            `start=task_${this.id}`

        const shareText = `\nЗадача от ${user.fullName()}.`

        return 'https://t.me/share/url?' +
            `url=${encodeURI(botLink)}&` +
            `text=${encodeURI(shareText)}`
    }
}

/**
 * Model structure
 */
Task.init({
    id: {
        type: DataTypes.STRING(15),
        primaryKey: true,
        autoIncrement: true,
    },
    author_id: {
        type: DataTypes.BIGINT,
        allowNull: false,
    },
    text: {
        type: DataTypes.TEXT,
        allowNull: false,
        set(value) {
            this.setDataValue('text', toHTML(value))
        },
    },
    details: {
        type: DataTypes.TEXT,
        set(value) {
            this.setDataValue('details', escapeHTML(value))
        },
    },
    target_id: {
        type: DataTypes.BIGINT,
        allowNull: false,
    },
    is_private: {
        type: DataTypes.SMALLINT,
        allowNull: false,
        defaultValue: 1,
    },
    is_notified: {
        type: DataTypes.SMALLINT,
        allowNull: false,
        defaultValue: 0,
    },
    is_completed: {
        type: DataTypes.SMALLINT,
        allowNull: false,
        defaultValue: 0,
    },
    file_id: {
        type: DataTypes.INTEGER,
    },
    execute_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    created_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    updated_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },
}, {
    sequelize,
    tableName: 'task',
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
})

/**
 * Get the author user that owns the task.
 */
Task.belongsTo(User, {
    foreignKey: 'author_id',
    as: 'author',
})

/**
 * Get the target user that owns the task.
 */
Task.belongsTo(User, {
    foreignKey: 'target_id',
    as: 'target',
})

/**
 * Get the tasks for the user.
 */
User.hasMany(Task, {
    foreignKey: 'author_id',
    as: 'tasks',
})

/**
 * Get the file that owns the task.
 */
Task.belongsTo(TaskFile, {
    foreignKey: 'file_id',
    as: 'file',
})

/**
 * Get the tasks for the file.
 */
TaskFile.hasMany(Task, {
    foreignKey: 'file_id',
    as: 'tasks',
})
