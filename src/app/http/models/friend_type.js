/**
 * Friendship type model
 */
import { Model, DataTypes } from 'sequelize'
import { sequelize } from '#connection'

/** Friendship type enum */
export const FRIEND_TYPE = {
    REQUEST: 'request',
    ACCEPT: 'accept',
    REJECT: 'reject',
    BLOCK: 'block',
}

/**
 * Extending model
 */
export class FriendType extends Model {  }

/**
 * Model structure
 */
FriendType.init({
    id: {
        type: DataTypes.STRING(50),
        primaryKey: true,
    },
    about: {
        type: DataTypes.STRING(250),
    },
}, {
    sequelize,
    tableName: 'friend_type',
    timestamps: false,
})
