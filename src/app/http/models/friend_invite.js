/**
 * Friend invite model
 */
import { Model, DataTypes } from 'sequelize'
import { sequelize } from '#connection'

/**
 * Extending model
 */
export class FriendInvite extends Model { }

/**
 * Model structure
 */
FriendInvite.init({
    id: {
        type: DataTypes.STRING(15),
        primaryKey: true,
    },
    from_id: {
        type: DataTypes.BIGINT,
        allowNull: false,
    },
    to_id: {
        type: DataTypes.BIGINT,
    },
    created_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    updated_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },
}, {
    sequelize,
    tableName: 'friend_invite',
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
})
