/**
 * Global setting model
 */
import { Model, DataTypes } from 'sequelize'
import { sequelize } from '#connection'

/** Global setting type enum */
export const GLOBAL_SETTING_TYPE = {
    //
}

/**
 * Extending model
 */
export class GlobalSetting extends Model {  }

/**
 * Model structure
 */
GlobalSetting.init({
    id: {
        type: DataTypes.STRING(50),
        primaryKey: true,
    },
    value: {
        type: DataTypes.TEXT,
        allowNull: false,
    },
    created_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    updated_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },
}, {
    sequelize,
    tableName: 'global_setting',
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
})
