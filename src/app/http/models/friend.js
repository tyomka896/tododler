/**
 * Friendship model
 */
import { Model, DataTypes } from 'sequelize'
import { sequelize } from '#connection'
import { FriendType } from './friend_type.js'

/**
 * Extending model
 */
export class Friend extends Model {  }

/**
 * Model structure
 */
Friend.init({
    from_id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
    },
    to_id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
    },
    type_id: {
        type: DataTypes.TEXT,
        allowNull: false,
    },
    created_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },
}, {
    sequelize,
    tableName: 'friend',
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: false,
})

/**
 * Get the FriendType that owns the Friend.
 */
Friend.belongsTo(FriendType, {
    foreignKey: 'type_id',
    as: 'type',
})

/**
 * Get the Friends for the FriendType.
 */
FriendType.hasMany(Friend, {
    foreignKey: 'type_id',
    as: 'friend',
})
