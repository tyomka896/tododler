/**
 * Task file type model
 */
import { Model, DataTypes } from 'sequelize'
import { sequelize } from '#connection'

/** Task type enum */
export const FILE_TYPE = {
    IMAGE: 'image',
    VOICE: 'voice',
}

/**
 * Extending model
 */
export class TaskFileType extends Model {  }

/**
 * Model structure
 */
TaskFileType.init({
    id: {
        type: DataTypes.STRING(50),
        primaryKey: true,
    },
    about: {
        type: DataTypes.TEXT,
    },
}, {
    sequelize,
    tableName: 'task_file_type',
    timestamps: false,
})
