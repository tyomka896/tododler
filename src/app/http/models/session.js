/**
 * Session model
 */
import { Model, DataTypes } from 'sequelize'
import { sequelize } from '#connection'

/**
 * Extending model
 */
export class Session extends Model {  }

/**
 * Model structure
 */
Session.init({
    user_id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
    },
    chat_id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
    },
    value: {
        type: DataTypes.TEXT,
        allowNull: false,
        defaultValue: '{}',
    },
    created_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    updated_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },
}, {
    sequelize,
    tableName: 'session',
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
})
