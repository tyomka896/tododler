/**
 * Task image model
 */
import { Model, DataTypes } from 'sequelize'

import { sequelize } from '#connection'
import { hiddenTextLink } from '#utils/helpers.js'
import { FILE_TYPE } from './task_file_type.js'

/**
 * Extending model
 */
export class TaskFile extends Model {
    get link() {
        if (! env('DOC_URL') || ! env('DOC_PATH')) return ''

        const _path = this.path.replace(env('DOC_PATH'), '')

        return hiddenTextLink(env('DOC_URL') + _path)
    }

    isImage() {
        return this.type_id === FILE_TYPE.IMAGE
    }

    isVoice() {
        return this.type_id === FILE_TYPE.VOICE
    }
}

/**
 * Model structure
 */
TaskFile.init({
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    name: {
        type: DataTypes.STRING(150),
        allowNull: false,
    },
    size: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
    },
    path: {
        type: DataTypes.STRING(260),
        allowNull: false,
    },
    type_id: {
        type: DataTypes.STRING(50),
        allowNull: false,
    },
    created_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    updated_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },
}, {
    sequelize,
    tableName: 'task_file',
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
})
