/**
 * Middleware for storing user's state
 */
import { Session } from '#models/session.js'

/** Local storage */
const cache = {}

/**
 * Get session
 */
async function getSession(ctx) {
    const keys = {
        user_id: ctx.from.id,
        chat_id: ctx.chat.id,
    }

    const { value } =
        await Session.findOne({ where: keys }) ||
        await Session.create(keys)

    return JSON.parse(value)
}

/**
 * Update session
 */
async function updateSession(ctx, value) {
    const keys = {
        user_id: ctx.from.id,
        chat_id: ctx.chat.id,
    }

    return await Session.update(
        { value: JSON.stringify(value) },
        { where: keys }
    )
}

export default async function(ctx, next) {
    if (! ctx.from || ctx.from.is_bot || ! ctx.chat) return next()

    const key = `${ctx.from.id}|${ctx.chat.id}`

    ctx.session = cache[key] || await getSession(ctx)

    return next().then(() => {
        cache[key] = ctx.session
        updateSession(ctx, ctx.session)
    })
}
