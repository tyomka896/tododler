/**
 * Main stage
 */
import { Scenes } from 'telegraf'

import { userScene } from './user/index.js'

export default new Scenes.Stage([
    userScene,
])
