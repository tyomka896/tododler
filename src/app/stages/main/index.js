/**
 * Scenes definition
 */
import { userScene } from './scene.js'

/**
 * User's input handler
 */
async function inputHandler(ctx) {
    if (! ctx.session.input) {
        return ctx.reply('Отправь /help для вывода полного списка команд.')
    }

    if (ctx.message.text === '/empty') {
        ctx.message.text = null
    }

    const { path, name } =  ctx.session.input

    const module = await import(path)

    if (module[name]) {
        return module[name](ctx)
    }
}

const inputHandlerRegex = /^([^\/]|\/empty).*/

userScene.hears(inputHandlerRegex, inputHandler)

/**
 * User's document upload
 */
async function documentHandler(ctx) {
    if (! ctx.session.file) return

    const { path, name } =  ctx.session.file

    const module = await import(path)

    if (module[name]) {
        return module[name](ctx)
    }
}

userScene.on([ 'document', 'photo', 'sticker', 'voice' ], documentHandler)

/**
 * Delete message action
 */
async function deleteMessage(ctx) {
    await ctx.answerCbQuery()

    await ctx.deleteMessage()
        .catch(() => {
            const { message_id } = ctx.update?.callback_query?.message

            const extra = message_id ?
                {
                    parse_mode: 'HTML',
                    reply_to_message_id: message_id,
                } :
                null

            ctx.reply(
                '⚠️ <i>Не удалось удалить сообщение, удалите его вручную</i>.',
                extra
            )
        })
}

userScene.action('delete-message', deleteMessage)

export { userScene }

export { isInput, onInput } from './hear.js'

export { isFile, aFile, onFile } from './file.js'
