/**
 * Handle user's raw input
 */
import { validateInst } from '#utils/helpers.js'

/**
 * Is input expected
 * @param {Context} ctx
 */
export function isInput(ctx) {
    validateInst(ctx, 'Context')

    return Boolean(ctx.session.input)
}

/**
 * Called module file path
 */
function modulePath() {
    const stackFile = new Error()
        .stack
        .split('\n')[3]

    const filePath = new RegExp('file:\/+(\/{1}.+[^:0-9]):[0-9]+:')
        .exec(stackFile)

    if (! filePath) return null

    return filePath[1]
}

/**
 * Change input handler for the user
 * @param {Context} ctx
 * @param {function|string|undefined} method
 */
export function onInput(ctx, method) {
    validateInst(ctx, 'Context')

    if (! ctx.session) return ctx

    if (
        ! method ||
        (typeof method !== 'string' &&
        typeof method !== 'function')
    ) {
        return delete ctx.session.input
    }

    const path = modulePath()

    if (! path) return ctx

    ctx.session.input = {
        path: path,
        name: typeof method === 'function' ? method.name : method,
    }

    return ctx
}
