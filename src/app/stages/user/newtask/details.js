/**
 * Details of the task
 */
import { onInput } from '#scene'
import { toHTML } from '#utils/entities.js'
import { dateDialog } from './date.js'

/** Dialog */
export async function detailsDialog(ctx) {
    onInput(ctx, detailsValue)

    return [
        'Введите дополнительную информацию к задаче.\n' +
        'Отправьте /empty, чтобы оставить пустым.',
        { parse_mode: 'HTML' }
    ]
}

/** Messages */
export async function detailsValue(ctx) {
    let value = ctx.message.text

    if (value && value.length > 1000) {
        return ctx.reply(
            'Дополнительно не должно превышать 1000 символов.\n' +
            '<i>Уменьшите объем текста.</i>',
            { parse_mode: 'HTML' }
        )
    }

    ctx.session._task.details = value && toHTML({ text: value })

    return ctx.reply(...await dateDialog(ctx))
}
