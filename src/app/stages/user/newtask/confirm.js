/**
 * Confirm creation of the task
 */
import { Markup } from 'telegraf'

import { userScene, onInput } from '#scene'
import dayjs from '#utils/dayjs.js'
import { toHTML } from '#utils/entities.js'
import { firstCapital } from '#utils/helpers.js'
import { User } from '#models/user.js'
import { TaskFile } from '#models/task_file.js'
import { FRIEND_TYPE } from '#models/friend_type.js'
import { FILE_TYPE } from '#models/task_file_type.js'
import { getAuth } from '#controllers/userController.js'
import { createTask } from '#controllers/taskController.js'
import { friendsStatus } from '#controllers/friendController.js'
import { DATE_FORMAT } from './index.js'

/** Keyboard */
export async function selectKeyboard(ctx) {
    if (! ctx.session._task) {
        return [
            'Сообщение уже устарело.\n' +
            'Отправьте /newtask для создания новой задачи.'
        ]
    }

    let _message = ''

    const auth = await getAuth(ctx)

    const { userToId, fileId, text, details, date } = ctx.session._task

    const userTo = await User.findByPk(userToId)

    const file = await TaskFile.findByPk(fileId)

    _message += file ? file.link : ''

    const executeAt = dayjs(date).utcOffset(auth.utc)

    const formattedDate = firstCapital(executeAt.format(DATE_FORMAT))

    if (+auth.id !== 1211871972) {
        _message += `Будет создана задача на <b>${formattedDate}</b>.\n\n`
    }
    else {
        _message +=
            '🤪 <i>Пиписика</i>, твоя задачка еще не создана 🤷‍♂️ ' +
            'Не забудь, пожалуйста нажать кнопочку <b>Создать</b> 👇 ' +
            'После чего твоя <u>важная задачка</u> ' +
            `выполнится в <b>${formattedDate}</b>.\n\n`
    }

    const taskInfo = []

    if (userTo) {
        taskInfo.push(`• Для: ${userTo.fullName(false)}`)
    }
    if (file && file.type_id === FILE_TYPE.VOICE) {
        taskInfo.push('• Голосовое сообщение')
    }
    if (text) {
        taskInfo.push(`• Описание: ${toHTML(text)}`)
    }
    if (details) {
        taskInfo.push(`• Детали: <i>${details}</i>`)
    }
    if (dayjs().diff(executeAt) >= 0) {
        taskInfo.push('\n❗️Время раньше текущей даты, уведомления не будет.')
    }

    _message += taskInfo.join('\n')

    return [
        _message,
        {
            ...Markup.inlineKeyboard([
                [
                    Markup.button.callback('« Назад', 'back-to-newtask-date'),
                    Markup.button.callback('Создать', 'newtask-confirm'),
                ],
            ]),
            parse_mode: 'HTML',
            disable_web_page_preview: ! file
        },
    ]
}

/** Actions */
userScene.action('newtask-confirm', async ctx => {
    if (
        ctx.session._task?.text === undefined ||
        ctx.session._task?.date === undefined
    ) {
        return ctx.editMessageText(
            'Сообщение уже устарело.\n' +
            'Отправьте /newtask для создания новой задачи.'
        )
    }

    const auth = await getAuth(ctx)

    const { userToId, fileId, text, details, date } = ctx.session._task

    const userTo = await User.findByPk(userToId)

    if (userTo) {
        const { status } = await friendsStatus(auth, userTo)

        if (status !== FRIEND_TYPE.ACCEPT) {
            return ctx.editMessageText(
                'Вы не можете создать задачу для пользователя ' +
                `<b>${userTo.fullName()}</b>`,
                { parse_mode: 'HTML' }
            )
        }
    }

    const executeAt = dayjs(date)

    const model = await createTask(ctx, {
        author_id: auth.id,
        text,
        details,
        target_id: userTo?.id || auth.id,
        file_id: fileId,
        execute_at: executeAt.toDate(),
    })

    let _message = 'Создана новая задача'

    if (userTo) {
        _message += ` для <i>${userTo.fullName(false)}</i>`
    }

    _message += ` на <b>${model.executionDate(auth.utc, DATE_FORMAT)}</b>.`

    if (model.isExpired()) {
        _message +=
            '\n\n❗️Время раньше текущей даты, уведомления не будет.'
    }

    onInput(ctx)

    delete ctx.session._task

    return ctx.editMessageText(
        _message,
        {
            ...Markup.inlineKeyboard([
                [ Markup.button.callback('К задаче »', `task-model-${model.id}`) ]
            ]),
            parse_mode: 'HTML',
        }
    )
})
