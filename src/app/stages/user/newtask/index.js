/**
 * New task dialog
 */
import { TaskFile } from '#models/task_file.js'
import { deleteTaskFile } from '#controllers/taskFileController.js'
import { textDialog } from './text.js'

export const DATE_FORMAT = 'dd, D MMMM, YYYY года, HH:mm (Z)'

/** Dialog */
export async function newTaskDialog(ctx) {
    if (ctx.session._task?.fileId) {
        const file = await TaskFile.findByPk(ctx.session._task?.fileId)

        if (file) await deleteTaskFile(file)
    }

    ctx.session._task = {}

    return await textDialog(ctx)
}
