/**
 * Date and time of execution of the task
 */
import { Markup } from 'telegraf'

import dayjs from '#utils/dayjs.js'
import { firstCapital } from '#utils/helpers.js'
import { userScene, onInput } from '#scene'
import { getAuth } from '#controllers/userController.js'
import { SETTING_TYPE } from '#models/user_setting_type.js'
import { DATE_FORMAT } from './index.js'
import { selectKeyboard } from './confirm.js'

/** Dialog */
export async function dateDialog(ctx) {
    onInput(ctx, dateInput)

    ctx.session._task.date = dayjs()
        .add(30 - dayjs().minute() % 30, 'm')
        .second(0)
        .millisecond(0)
        .format()

    return await dateDialogKeyboard(ctx)
}

/** Keyboard */
async function dateDialogKeyboard(ctx) {
    const auth = await getAuth(ctx)

    const executeAt = dayjs(ctx.session._task.date).utcOffset(auth.utc)

    const formattedDate = firstCapital(executeAt.format(DATE_FORMAT))

    const yesterday = executeAt.subtract(1, 'day').format('dd, DD MMM')
    const tomorrow = executeAt.add(1, 'day').format('dd, DD MMM')

    let times = await auth.getItemValue(
        SETTING_TYPE.TIMES,
        [['10:00', '14:00', '18:00']]
    )

    if (typeof times === 'string') {
        times = JSON.parse(times)
    }

    times = times.map(elem =>
        elem.map(_elem =>
            Markup.button.callback(
                _elem,
                `newtask-time-${_elem}`
            )
        )
    )

    return [
        `Задача на <b>${formattedDate}</b>.\n\n` +
        '⏰ <i>Выберите дату выполнения задачи ' +
        'или отправьте ее текстом. Например: ' +
        `<code>${executeAt.format('HH:mm')}</code> (время), ` +
        `<code>${executeAt.format('DD.MM.YYYY')}</code> (дата) или ` +
        `<code>${executeAt.format('HH:mm DD.MM')}</code> (время и дата)</i>`,
        {
            ...Markup.inlineKeyboard([
                [
                    Markup.button.callback(
                        `« ${firstCapital(yesterday)}`,
                        'newtask-edit-date-sub-day'
                    ),
                    Markup.button.callback(
                        `${firstCapital(tomorrow)} »`,
                        'newtask-edit-date-add-day'
                    ),
                ],
                [
                    Markup.button.callback(
                        '« 1 мес.',
                        'newtask-edit-date-sub-month'
                    ),
                    Markup.button.callback(
                        '« 1 нед.',
                        'newtask-edit-date-sub-week'
                    ),
                    Markup.button.callback(
                        '1 нед. »',
                        'newtask-edit-date-add-week'
                    ),
                    Markup.button.callback(
                        '1 мес. »',
                        'newtask-edit-date-add-month'
                    ),
                ],
                ...times,
                [Markup.button.callback('Выбрать', 'newtask-select')],
            ]),
            parse_mode: 'HTML',
        }
    ]
}

/** Actions */
userScene.action(/newtask-edit-date-(sub|add)-(day|week|month)/, async ctx => {
    const operation = ctx.match[1]
    const dayRange = ctx.match[2]

    const auth = await getAuth(ctx)

    let executeAt = dayjs(ctx.session._task.date)

    if (dayRange === 'day') {
        executeAt = executeAt.add(operation === 'add' ? 1 : -1, 'day')
    }
    else if (dayRange === 'week') {
        executeAt = executeAt.add(operation === 'add' ? 1 : -1, 'week')
    }
    else if (dayRange === 'month') {
        executeAt = executeAt.add(operation === 'add' ? 1 : -1, 'month')
    }

    ctx.session._task.date = executeAt.utcOffset(auth.utc).format()

    return ctx.editMessageText(...await dateDialogKeyboard(ctx))
})

userScene.action(/newtask-time-(\d{1,2}[\:]\d{2})/, async ctx => {
    await ctx.answerCbQuery()

    const auth = await getAuth(ctx)

    let executeAt = dayjs(ctx.session._task.date).utcOffset(auth.utc)

    const [hour, minute] = ctx.match[1].split(':')

    if (executeAt.hour() == hour && executeAt.minute() == minute) return

    executeAt = executeAt.hour(hour).minute(minute)

    ctx.session._task.date = executeAt.format()

    return ctx.editMessageText(...await dateDialogKeyboard(ctx))
})

userScene.action('newtask-select', async ctx => {
    return ctx.editMessageText(...await selectKeyboard(ctx))
})

userScene.action('back-to-newtask-date', async ctx => {
    onInput(ctx, dateInput)

    return ctx.editMessageText(...await dateDialogKeyboard(ctx))
})

/** Messages */

/** Input date and time validation */
const dateRegex = {
    // HH:mm
    time: /\d{1,2}[\:]\d{2}.*/,
    // DD.MM
    dayMonth: /\d{1,2}[\.]\d{1,2}/,
    // DD.MM.YYYY
    fullDate: /\d{1,2}[\.]\d{1,2}[\.]\d{4}/,
    // HH:mm DD.MM
    timeDayMonth: /\d{1,2}[\:]\d{2}\s\d{1,2}[\.]\d{1,2}.*/,
    // HH:mm DD.MM.YYYY
    full: /\d{1,2}[\:]\d{2}\s\d{1,2}[\.]\d{1,2}[\.]\d{4}/,
}

export async function dateInput(ctx) {
    let value = ctx.message.text

    if (!value) {
        return ctx.reply('Дата не может быть пустой.')
    }

    const auth = await getAuth(ctx)

    value = value.replace(/\r?\n/g, '')

    let executeAt = dayjs(ctx.session._task.date).utcOffset(auth.utc)
    const parts = value.split((/[\.\:\s]/))

    if (dateRegex.fullDate.test(value) || dateRegex.full.test(value)) {
        executeAt = executeAt.year(parts.pop())
    }
    if (dateRegex.dayMonth.test(value) || dateRegex.timeDayMonth.test(value)) {
        executeAt = executeAt.month(+parts.pop() - 1).date(parts.pop())
    }
    if (dateRegex.time.test(value)) {
        executeAt = executeAt.minute(parts.pop()).hour(parts.pop())
    }

    if (parts.length !== 0) {
        return ctx.reply('Дата введена не верно, проверьте формат.')
    }
    if (executeAt.year() <= 1970) {
        return ctx.reply('Ну и какой смысл создавать задачу за этот год 😏')
    }
    if (executeAt.diff(dayjs(), 'years') >= 10) {
        const maxYear = dayjs().add(10, 'years').format('YYYY')

        return ctx.reply(`Нельзя создать задачу дальше ${maxYear} года.`)
    }

    ctx.session._task.date = executeAt.format()

    return ctx.reply(...await selectKeyboard(ctx))
}
