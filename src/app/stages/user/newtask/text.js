/**
 * Name of the task
 */
import { onInput, onFile } from '#scene'
import {
    downloadDocument,
    downloadPhoto,
    downloadSticker,
    downloadVoice,
} from '#utils/download.js'
import { TaskFile } from '#models/task_file.js'
import { FILE_TYPE } from '#models/task_file_type.js'
import { SETTING_TYPE } from '#models/user_setting_type.js'
import { getAuth } from '#controllers/userController.js'
import { detailsDialog } from './details.js'
import { dateDialog } from './date.js'

/** Dialog */
export async function textDialog(ctx) {
    onInput(ctx, textInput)
    onFile(ctx, textFileInput)

    return [
        'Отправьте <u><i>текст с картинкой</i></u> ' +
        'или минутное <i>голосовое сообщение</i>.',
        { parse_mode: 'HTML' }
    ]
}

/** Messages */
export async function textInput(ctx) {
    const message = await validateText(ctx)

    if (! message) return

    ctx.session._task.text = message

    onFile(ctx)

    const auth = await getAuth(ctx)

    const onlyTextItem = await auth.getItemValue(SETTING_TYPE.ONLY_TEXT, true)

    if (+onlyTextItem) {
        return ctx.reply(...await dateDialog(ctx))
    }

    return ctx.reply(...await detailsDialog(ctx))
}

/** Documents */
export async function textFileInput(ctx) {
    const info = await validateFile(ctx)

    if (! info) return

    const file = await TaskFile.create({
        name: info.name,
        size: info.size || 0,
        path: info.path.replace(/\\/g, '/'),
        type_id: info.type,
    })

    ctx.session._task.fileId = file.id

    onFile(ctx)

    if (file.type_id === FILE_TYPE.VOICE) {
        ctx.session._task.text = ''
    }
    else {
        const message = await validateText(ctx)

        if (! message) return

        ctx.session._task.text = message
    }

    const auth = await getAuth(ctx)

    const onlyTextItem = await auth.getItemValue(SETTING_TYPE.ONLY_TEXT, true)

    if (+onlyTextItem) {
        return ctx.reply(...await dateDialog(ctx))
    }

    return ctx.reply(...await detailsDialog(ctx))
}

/** Validation */
export async function validateText(ctx) {
    let message = null

    const value = ctx.message.text || ctx.message.caption

    if (! value) {
        await ctx.reply('Описание не может быть пустым.')
    }
    else if (value.length > 1000) {
        await ctx.reply(
            'Описание не должно превышать 1000 символов.\n' +
            '<i>Уменьшите объем текста.</i>',
            { parse_mode: 'HTML' }
        )
    }
    else {
        message = {
            text: value,
            entities: ctx.message.entities ||
                ctx.message.caption_entities,
        }
    }

    return message
}

export async function validateFile(ctx) {
    let fileInfo = null

    if (ctx.message.document) {
        const { mime_type, file_size } = ctx.message.document

        if (! mime_type.includes('image')) {
            await ctx.reply('Можно отправить только изображение.')
        }
        else if (file_size >= 1048576) {
            await ctx.reply('Изображение должно быть не больше мегабайта.')
        }
        else {
            fileInfo = await downloadDocument(ctx)
        }
    }
    else if (ctx.message.photo) {
        fileInfo = await downloadPhoto(ctx)
    }
    else if (ctx.message.sticker) {
        const { is_animated, is_video, file_size } = ctx.message.sticker

        if (is_animated || is_video) {
            await ctx.reply('Анимированные изображения не допускаются.')
        }
        else if (file_size >= 1048576) {
            await ctx.reply('Изображение должно быть не больше мегабайта.')
        }
        else {
            fileInfo = await downloadSticker(ctx)
        }
    }
    else if (ctx.message.voice) {
        const { duration, mime_type } = ctx.message.voice

        if (! mime_type.includes('ogg')) {
            await ctx.reply('Можно отправить только голосовое сообщение.')
        }
        else if (duration > 60) {
            await ctx.reply('Голосовое сообщение должно быть не дольше минуты.')
        }
        else {
            fileInfo = await downloadVoice(ctx)
            fileInfo.type = FILE_TYPE.VOICE
        }
    }
    else {
        await ctx.reply('Что это вообще такое мне тут подсовывается 💁‍♂️')
    }

    if (fileInfo && ! fileInfo.type) {
        fileInfo.type = FILE_TYPE.IMAGE
    }

    return fileInfo
}
