/**
 * Status scene
 */
import { Markup } from 'telegraf'
import schedule from 'node-schedule'

import dayjs from '#utils/dayjs.js'
import { userScene } from '#scene'
import { User } from '#models/user.js'
import { Task } from '#models/task.js'
import { TaskFile } from '#models/task_file.js'

/**
 * Format bytes to measurement value
 * @param {number} bytes
 * @param {number} decimals
 * @returns
 */
function formatBytes(bytes, decimals = 2) {
    if (!+bytes) return '0 Bytes'

    const k = 1024
    const dm = decimals < 0 ? 0 : decimals
    const sizes = [ 'Bytes', 'KiB', 'MiB', 'GiB' ]

    const i = Math.floor(Math.log(bytes) / Math.log(k))

    return `${parseFloat((bytes / Math.pow(k, i)).toFixed(dm))} ${sizes[i]}`
}

/** Keyboard */
export async function statusKeyboard(ctx) {
    let rows = []
    const params = ctx.message?.text.split(' ').splice(1) || []

    if (params.length > 0) {
        const _command = params[0].toString().toLowerCase()

        if (_command === 'users') {
            rows = await usersCommand(params)
        }
        else if (_command === 'jobs') {
            rows = await jobsCommand()
        }

        if (rows.length > 0) {
            return [
                rows.join(''),
                { parse_mode: 'HTML' },
            ]
        }
    }

    const tasksTotal = await Task.count()
    const tasksCompleted = await Task.count({ where: { is_completed: 1 } })
    const completedRation = Math.round(tasksCompleted / tasksTotal * 100) / 100
    const filesTotal = await TaskFile.count()
    const filesSize = await TaskFile.sum('size')

    rows = [
        `<b>Users</b> : ${await User.count()}\n`,
        `<b>Tasks</b> : ${tasksTotal} (<i>${completedRation}</i>)\n` +
        `<b>Files</b> : ${filesTotal} (<i>${formatBytes(filesSize)}</i>)\n` +
        `<b>Jobs</b> : ${Object.keys(schedule.scheduledJobs).length}`,
    ]

    return [
        rows.join(''),
        {
            ...Markup.inlineKeyboard([
                [ Markup.button.callback('Обновить', 'status-update') ],
            ]),
            parse_mode: 'HTML'
        },
    ]
}

/** Commands */
async function usersCommand(params) {
    let _limit =
        params[1] && params[1] > 0 && params[1] <= 10 ?
        params[1] : 3

    let _users = await User.findAll({
        order: [ [ 'created_at', 'desc' ] ],
        limit: _limit,
    })

    _users = _users.map(elem => {
        const createAt = dayjs(elem.created_at)
            .utcOffset(elem.utc || '+00:00')
            .format()

        return `• ${elem.fullName()}\n` +
        `  <i>${createAt}</i>\n`
    })

    return [
        `<b>Last ${_limit} users</b>:\n\n`,
        _users.join('')
    ]
}

async function jobsCommand() {
    const _jobs = Object.keys(schedule.scheduledJobs)
        .map(elem => `• ${elem}\n`)

    return [
        '<b>List of jobs</b>:\n\n',
        _jobs.join('')
    ]
}

/** Actions */
userScene.action('status-update', async ctx => {
    await ctx.editMessageText(...await statusKeyboard(ctx))
        .catch(() => {})

    await ctx.answerCbQuery()
})
