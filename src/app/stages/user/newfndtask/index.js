/**
 * New task for friend
 */
import { Markup } from 'telegraf'

import { userScene } from '#scene'
import { User } from '#models/user.js'
import { Friend } from '#models/friend.js'
import { FRIEND_TYPE } from '#models/friend_type.js'
import { getAuth } from '#controllers/userController.js'
import { friendsStatus } from '#controllers/friendController.js'
import { newTaskDialog } from '../newtask/index.js'

/** Keyboard */
export async function friendsTaskKeyboard(ctx) {
    const auth = await getAuth(ctx)

    const ROWS_PER_PAGE = 5

    let page = 0
    let offset = 0

    if (ctx.match && /(-page-|back-to-)/.test(ctx.match[0])) {
        if (ctx.match[1]) {
            page = +ctx.match[1]
        }
        else if (ctx.session.friendsPage) {
            page = +ctx.session.friendsPage
        }

        offset = page * ROWS_PER_PAGE
    }

    const modelsCount = await Friend.count({ where: { from_id: auth.id } })

    if (modelsCount === 0) {
        return [
            'Список друзей пуст.\n' +
            'Отправьте /addfnd чтобы добавить первого друга.'
        ]
    }

    const models = (await auth.getFriends({
        order: [[ 'created_at', 'desc' ]],
        offset: offset,
        limit: ROWS_PER_PAGE,
        include: [ 'toUser' ],
    }))
        .map(elem => ([
            Markup.button.callback(
                elem.toUser.fullName(false),
                `task-to-friend-model-${elem.toUser.id}`
            )
        ]))

    return [
        'Задача для друга ' +
        `(${Math.min(offset + ROWS_PER_PAGE, modelsCount)}/${modelsCount})`,
        Markup.inlineKeyboard([
            ...models,
            [
                Markup.button.callback(
                    '«',
                    `task-to-friends-page-${page - 1}`,
                    page === 0
                ),
                Markup.button.callback(
                    '»',
                    `task-to-friends-page-${page + 1}`,
                    ROWS_PER_PAGE >= modelsCount - offset
                ),
            ],
        ])
    ]
}

/** Actions */
userScene.action(/task-to-friends-page-(\d+)/, async ctx => {
    ctx.session.friendsPage = +ctx.match[1]

    return ctx.editMessageText(...await friendsTaskKeyboard(ctx))
})

userScene.action(/task-to-friend-model-(\d+)/, async ctx => {
    const fromUser = await getAuth(ctx)

    const toUser = await User.findByPk(+ctx.match[1])

    const { message, status } = await getUsersStatus(fromUser, toUser)

    if (status !== FRIEND_TYPE.ACCEPT && message) {
        return ctx.editMessageText(
            message,
            {
                ...Markup.inlineKeyboard([
                    [
                        Markup.button.callback(
                            '« Назад', 
                            'back-to-task-to-friends'
                        )
                    ]
                ]),
                parse_mode: 'HTML',
            }
        )
    }
    await ctx.answerCbQuery()

    const taskDialog = await newTaskDialog(ctx)

    ctx.session._task.userToId = +ctx.match[1]

    return ctx.reply(...taskDialog)
})

async function getUsersStatus(fromUser, toUser) {
    const { status, initiator } = await friendsStatus(fromUser, toUser)

    let _message = null

    switch (status) {
        case FRIEND_TYPE.REQUEST:
            _message = fromUser.id === initiator.id ?
                'Вы отправили запрос дружбы, ожидайте ответа.' :
                null
            break
        case FRIEND_TYPE.REJECT:
            _message = fromUser.id === initiator.id ?
                null :
                `Пользователь <b>${toUser.fullName()}</b> ` +
                'отклонил запрос дружбы.'
            break
        case FRIEND_TYPE.BLOCK:
            _message = fromUser.id === initiator.id ?
                'Вы заблокировали этого пользователя.' :
                'Пользователь вас заблокировал.'
            break
    }

    return {
        status,
        initiator,
        message: _message,
    }
}

userScene.action('back-to-task-to-friends', async ctx => {
    return ctx.editMessageText(...await friendsTaskKeyboard(ctx))
})
