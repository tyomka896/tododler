/**
 * User's tasks
 */
import { Markup } from 'telegraf'

import { userScene } from '#scene'
import { Friend } from '#models/friend.js'
import { getAuth } from '#controllers/userController.js'
import { friendEditKeyboard } from './edit.js'
import { addFndDialog } from '#app/stages/user/addfnd/index.js'

/** Keyboard */
export async function friendsKeyboard(ctx) {
    const auth = await getAuth(ctx)

    const ROWS_PER_PAGE = 5

    let page = 0
    let offset = 0

    if (ctx.match && /(-page-|back-to-)/.test(ctx.match[0])) {
        if (ctx.match[1]) {
            page = +ctx.match[1]
        }
        else if (ctx.session.friendsPage) {
            page = +ctx.session.friendsPage
        }

        offset = page * ROWS_PER_PAGE
    }

    const models = (await auth.getFriends({
        order: [['created_at', 'desc']],
        offset: offset,
        limit: ROWS_PER_PAGE,
        include: ['toUser'],
    }))
        .map(elem => ([
            Markup.button.callback(
                elem.toUser.fullName(false),
                `friend-model-${elem.toUser.id}`
            )
        ]))

    const modelsCount = await Friend.count({ where: { from_id: auth.id } })

    return [
        'Мои друзья ' +
        `(${Math.min(offset + ROWS_PER_PAGE, modelsCount)}/${modelsCount})`,
        Markup.inlineKeyboard([
            ...models,
            [
                Markup.button.callback(
                    '«',
                    `friends-page-${page - 1}`,
                    page === 0
                ),
                Markup.button.callback(
                    '»',
                    `friends-page-${page + 1}`,
                    ROWS_PER_PAGE >= modelsCount - offset
                ),
            ],
            [
                Markup.button.callback('Добавить', 'friend-add-new'),
            ],
        ])
    ]
}

/** Actions */
userScene.action(/friends-page-(\d+)/, async ctx => {
    ctx.session.friendsPage = +ctx.match[1]

    return ctx.editMessageText(...await friendsKeyboard(ctx))
})

userScene.action(/friend-model-(\d+)/, async ctx => {
    ctx.session.friendId = +ctx.match[1]

    return ctx.editMessageText(...await friendEditKeyboard(ctx))
})

userScene.action('friend-add-new', async ctx => {
    return ctx.editMessageText(...await addFndDialog(ctx))
})

userScene.action('back-to-friends', async ctx => {
    return ctx.editMessageText(...await friendsKeyboard(ctx))
})
