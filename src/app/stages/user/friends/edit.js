/**
 * Task edit
 */
import { Markup } from 'telegraf'

import { userScene } from '#scene'
import { User } from '#models/user.js'
import { FRIEND_TYPE } from '#models/friend_type.js'
import { getAuth } from '#controllers/userController.js'
import { friendsStatus } from '#controllers/friendController.js'
import { newTaskDialog } from '#app/stages/user/newtask/index.js'

/** Keyboard */
export async function friendEditKeyboard(ctx) {
    const fromUser = await getAuth(ctx)

    const toUser = await User.findByPk(ctx.session.friendId)

    const { message, status } = await getUsersStatus(fromUser, toUser)

    const notFriend = status !== FRIEND_TYPE.ACCEPT

    return [
        message || toUser.fullName(),
        {
            ...Markup.inlineKeyboard([
                [
                    Markup.button.callback('« Назад', 'back-to-friends'),
                    Markup.button.callback(
                        'Задача',
                        `friend-create-task-${toUser.id}`,
                        notFriend
                    )
                ],
            ]),
            parse_mode: 'HTML',
        }
    ]
}

async function getUsersStatus(fromUser, toUser) {
    const { status, initiator } = await friendsStatus(fromUser, toUser)

    let _message = null

    switch (status) {
        case FRIEND_TYPE.REQUEST:
            _message = fromUser.id === initiator.id ?
                'Вы отправили запрос дружбы, ожидайте ответа.' :
                null
            break
        case FRIEND_TYPE.REJECT:
            _message = fromUser.id === initiator.id ?
                null :
                `Пользователь <b>${toUser.fullName()}</b> ` +
                'отклонил запрос дружбы.'
            break
        case FRIEND_TYPE.BLOCK:
            _message = fromUser.id === initiator.id ?
                'Вы заблокировали этого пользователя.' :
                'Пользователь вас заблокировал.'
            break
    }

    return {
        status,
        initiator,
        message: _message,
    }
}

/** Actions */
userScene.action(/friend-create-task-(\d+)/, async ctx => {
    await ctx.answerCbQuery()

    const taskDialog = await newTaskDialog(ctx)

    ctx.session._task.userToId = +ctx.match[1]

    return ctx.reply(...taskDialog)
})
