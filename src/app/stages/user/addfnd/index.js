/**
 * Add friend dialog
 */
import { Markup } from 'telegraf'

import {
    getInviteToken,
    tokenShareLink,
} from '#controllers/friendInviteController.js'

/** Dialog */
export async function addFndDialog(ctx) {
    const token = await getInviteToken(ctx)

    const link = await tokenShareLink(token)

    return [
        'Поделитесь ссылкой ниже с новым другом.',
        {
            ...Markup.inlineKeyboard([
                [ Markup.button.url('Пригласить', link) ]
            ]),
            parse_mode: 'HTML',
        }
    ]
}
