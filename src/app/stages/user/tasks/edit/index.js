/**
 * Task edit
 */
import { Markup } from 'telegraf'

import { userScene } from '#scene'
import dayjs from '#utils/dayjs.js'
import { backToModels } from '#utils/helpers.js'
import { User } from '#models/user.js'
import { Task } from '#models/task.js'
import { FILE_TYPE } from '#models/task_file_type.js'
import { getAuth } from '#controllers/userController.js'
import { scheduleTask, completeTask, deleteTask } from '#controllers/taskController.js'

import './text.js'
import './details.js'
import './image.js'
import './execute/index.js'

/** Keyboard */
export async function taskEditKeyboard(ctx) {
    let _message = ''

    const auth = await getAuth(ctx)

    const task = await Task.findByPk(ctx.session.taskId)

    const file = await task.getFile()

    _message += file ? file.link : ''

    if (auth.id === task.target_id) {
        _message += `Моя задача от <b>${task.executionDate(auth.utc)}</b>`

        if (auth.id !== task.author_id) {
            const userFrom = await task.getAuthor()

            _message += `\nОт <b>${userFrom.fullName(false)}</b>`
        }
    }
    else {
        const userTo = await User.findByPk(task.target_id)

        _message += `Задача от <b>${task.executionDate(auth.utc)}</b>\n`
        _message += `Для <b>${userTo.fullName(false)}</b>`

        if (!task.is_notified && !task.is_completed) {
            _message += ' ожидает уведомления'
        }
        else if (task.is_notified && !task.is_completed) {
            _message += ' не завершена'
        }
        else if (task.is_completed) {
            _message += ' завершена'
        }
    }

    _message += `\n\n${task.textAndDetails()}`

    const cannotEdit =
        auth.id !== task.author_id ||
        task.is_notified ||
        task.is_completed

    return [
        _message,
        {
            ...Markup.inlineKeyboard([
                [
                    Markup.button.callback(
                        'Описание',
                        `task-edit-text-${task.id}`,
                        cannotEdit
                    ),
                    Markup.button.callback(
                        'Доп. информация',
                        `task-edit-details-${task.id}`,
                        cannotEdit
                    ),
                ],
                [
                    Markup.button.callback(
                        'Картинка',
                        `task-edit-image-${task.id}`,
                        cannotEdit || file?.type_id === FILE_TYPE.VOICE
                    ),
                    Markup.button.callback(
                        'Перенести',
                        `task-edit-execute-${task.id}`,
                        auth.id !== task.author_id || task.is_completed
                    ),
                ],
                [
                    Markup.button.url(
                        'Поделиться',
                        task.shareLink(auth),
                        auth.id !== task.author_id || task.is_completed
                    ),
                    Markup.button.callback(
                        'Возобновить',
                        `task-reopen-${task.id}`,
                        auth.id !== task.target_id ||
                        !task.is_completed ||
                        dayjs().diff(task.updated_at, 'days') >= 30
                    ),
                ],
                [
                    Markup.button.callback('« Назад', 'back-to-tasks'),
                    Markup.button.callback(
                        'Завершить',
                        `task-complete-${task.id}`,
                        auth.id !== task.target_id || task.is_completed
                    ),
                    Markup.button.callback(
                        'Удалить',
                        `task-delete-${task.id}`,
                        auth.id !== task.target_id || !task.is_completed
                    )
                ],
            ]),
            parse_mode: 'HTML',
            disable_web_page_preview: !task.file_id,
        }
    ]
}

/** Actions */
userScene.action(/task-reopen-(\w{10,})/, async ctx => {
    const model = await Task.findByPk(ctx.match[1])

    if (!model) {
        return backToModels(ctx, 'Задача не найдена.', 'tasks')
    }

    return ctx.editMessageText(
        'Точно возобновить задачу?',
        Markup.inlineKeyboard([
            [
                Markup.button.callback('Нет', `task-model-${model.id}`),
                Markup.button.callback('Да', `task-reopen-yes-${model.id}`),
            ],
        ])
    )
})

userScene.action(/task-reopen-yes-(\w{10,})/, async ctx => {
    const model = await Task.findByPk(ctx.match[1])

    if (!model) {
        return backToModels(ctx, 'Задача не найдена.', 'tasks')
    }

    await model.update({
        is_notified: 0,
        is_completed: 0,
    })

    await scheduleTask(ctx, model)

    return ctx.editMessageText(...await taskEditKeyboard(ctx))
})

userScene.action(/task-complete-(\w{10,})/, async ctx => {
    const model = await Task.findByPk(ctx.match[1])

    if (!model) {
        return backToModels(ctx, 'Задача не найдена.', 'tasks')
    }

    return ctx.editMessageText(
        'Точно завершить задачу?',
        Markup.inlineKeyboard([
            [
                Markup.button.callback('Нет', `task-model-${model.id}`),
                Markup.button.callback('Да', `task-complete-yes-${model.id}`),
            ],
        ])
    )
})

userScene.action(/task-complete-yes-(\w{10,})/, async ctx => {
    const model = await Task.findByPk(ctx.match[1])

    if (!model) {
        return backToModels(ctx, 'Задача не найдена.', 'tasks')
    }

    await completeTask(model)

    return ctx.editMessageText(...await taskEditKeyboard(ctx))
})

userScene.action(/task-delete-(\w{10,})/, async ctx => {
    const model = await Task.findByPk(ctx.match[1])

    if (!model) {
        return backToModels(ctx, 'Задача не найдена.', 'tasks')
    }

    return ctx.editMessageText(
        'Вы уверены, что хотите удалить задачу?',
        Markup.inlineKeyboard([
            [
                Markup.button.callback('Нет', `task-model-${model.id}`),
                Markup.button.callback('Да', `task-delete-yes-${model.id}`),
            ],
        ])
    )
})

userScene.action(/task-delete-yes-(\w{10,})/, async ctx => {
    const model = await Task.findByPk(ctx.match[1])

    if (!model) {
        return backToModels(ctx, 'Задача не найдена.', 'tasks')
    }

    await deleteTask(model)

    return backToModels(ctx, 'Задача успешно удалена.', 'tasks')
})
