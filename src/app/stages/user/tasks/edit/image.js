/**
 * Task image edit
 */
import { Markup } from 'telegraf'

import { userScene, onInput, onFile } from '#scene'
import { backToModels } from '#utils/helpers.js'
import { Task } from '#models/task.js'
import { updateTaskFile, emptyTaskFile } from '#controllers/taskController.js'
import { validateFile } from '#app/stages/user/newtask/text.js'
import { taskEditKeyboard } from './index.js'

/** Actions */
userScene.action(/task-edit-image-(\w{10,})/, async ctx => {
    await ctx.answerCbQuery()

    const model = await Task.findByPk(ctx.match[1])

    if (!model) {
        return backToModels(ctx, 'Задача не найдена.', 'tasks')
    }

    if (model.is_notified || model.is_completed) {
        return ctx.editMessageText(
            'Задача уже используется, ее нельзя редактировать.',
            Markup.inlineKeyboard([
                [Markup.button.callback('« Назад', `task-model-${model.id}`)]
            ])
        )
    }

    onInput(ctx, emptyImage)
    onFile(ctx, uploadImage)

    return ctx.reply(
        'Отправьте изображение для задачи.\n' +
        'Отправьте /empty, чтобы оставить пустым.'
    )
})

/** Messages */
export async function emptyImage(ctx) {
    let value = ctx.message.text

    if (value) return

    const model = await Task.findByPk(ctx.session.taskId)

    if (!model) {
        return backToModels(ctx, 'Задача не найдена.', 'tasks', 0)
    }

    await emptyTaskFile(model)

    onInput(ctx)
    onFile(ctx)

    return ctx.reply(...await taskEditKeyboard(ctx))
}

/** Documents */
export async function uploadImage(ctx) {
    const model = await Task.findByPk(ctx.session.taskId)

    if (!model) {
        return backToModels(ctx, 'Задача не найдена.', 'tasks')
    }

    const info = await validateFile(ctx)

    if (!info) return

    await updateTaskFile(model, info)

    onInput(ctx)
    onFile(ctx)

    return ctx.reply(...await taskEditKeyboard(ctx))
}
