/**
 * Task details edit
 */
import { Markup } from 'telegraf'

import { userScene, onInput } from '#scene'
import { backToModels } from '#utils/helpers.js'
import { Task } from '#models/task.js'
import { taskEditKeyboard } from './index.js'

/** Actions */
userScene.action(/task-edit-details-(\w{10,})/, async ctx => {
    await ctx.answerCbQuery()

    const model = await Task.findByPk(ctx.match[1])

    if (!model) {
        return backToModels(ctx, 'Задача не найдена.', 'tasks')
    }

    if (model.is_notified || model.is_completed) {
        return ctx.editMessageText(
            'Задача уже используется, ее нельзя редактировать.',
            Markup.inlineKeyboard([
                [Markup.button.callback('« Назад', `task-model-${model.id}`)]
            ])
        )
    }

    onInput(ctx, updateDetails)

    return ctx.reply(
        'Введите новую дополнительную информацию задачи.' +
        'Отправьте /empty, чтобы оставить пустым.'
    )
})

/** Messages */
export async function updateDetails(ctx) {
    let value = ctx.message.text

    if (value && value.length > 1000) {
        return ctx.reply('Дополнительно не должно превышать 1000 символов.')
    }

    const model = await Task.findByPk(ctx.session.taskId)

    if (!model) {
        return backToModels(ctx, 'Задача не найдена.', 'tasks', 0)
    }

    await model.update({ details: value })

    onInput(ctx)

    return ctx.reply(...await taskEditKeyboard(ctx))
}
