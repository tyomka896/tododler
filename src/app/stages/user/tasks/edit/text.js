/**
 * Task text edit
 */
import { Markup } from 'telegraf'

import { userScene, onInput, onFile } from '#scene'
import { backToModels } from '#utils/helpers.js'
import { Task } from '#models/task.js'
import { FILE_TYPE } from '#models/task_file_type.js'
import { updateTaskFile, emptyTaskFile } from '#controllers/taskController.js'
import { taskEditKeyboard } from './index.js'
import { validateText, validateFile } from '#app/stages/user/newtask/text.js'

/** Actions */
userScene.action(/task-edit-text-(\w{10,})/, async ctx => {
    await ctx.answerCbQuery()

    const model = await Task.findByPk(ctx.match[1])

    if (!model) {
        return backToModels(ctx, 'Задача не найдена.', 'tasks')
    }

    if (model.is_notified || model.is_completed) {
        return ctx.editMessageText(
            'Задача уже используется, ее нельзя редактировать.',
            Markup.inlineKeyboard([
                [Markup.button.callback('« Назад', `task-model-${model.id}`)]
            ])
        )
    }

    onInput(ctx, updateText)
    onFile(ctx, updateTextFileInput)

    return ctx.reply('Введите новое описание задачи.')
})

/** Messages */
export async function updateText(ctx) {
    const model = await Task.findByPk(ctx.session.taskId)

    if (!model) {
        return backToModels(ctx, 'Задача не найдена.', 'tasks', 0)
    }

    const message = await validateText(ctx)

    if (!message) return

    await model.update({ text: message })

    const file = await model.getFile()

    if (file && file.type_id === FILE_TYPE.VOICE) {
        await emptyTaskFile(model)
    }

    onInput(ctx)
    onFile(ctx)

    return ctx.reply(...await taskEditKeyboard(ctx))
}

export async function updateTextFileInput(ctx) {
    const model = await Task.findByPk(ctx.session.taskId)

    if (!model) {
        return backToModels(ctx, 'Задача не найдена.', 'tasks', 0)
    }

    const info = await validateFile(ctx)

    if (!info) return

    await updateTaskFile(model, info)

    if (info.type === FILE_TYPE.VOICE) {
        await model.update({ text: '' })
    }
    else {
        const message = await validateText(ctx)

        if (!message) return

        await model.update({ text: message })
    }

    onInput(ctx)
    onFile(ctx)

    return ctx.reply(...await taskEditKeyboard(ctx))
}
