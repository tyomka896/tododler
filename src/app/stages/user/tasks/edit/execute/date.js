/**
 * Date and time of execution of the task
 */
import { Markup } from 'telegraf'

import { userScene, onInput } from '#scene'
import dayjs from '#utils/dayjs.js'
import { firstCapital } from '#utils/helpers.js'
import { Task } from '#models/task.js'
import { SETTING_TYPE } from '#models/user_setting_type.js'
import { getAuth } from '#controllers/userController.js'
import { scheduleTask } from '#controllers/taskController.js'

/** Dialog */
export async function dateEditDialog(ctx) {
    onInput(ctx, dateInput)

    const task = await Task.findByPk(ctx.match[1])

    ctx.session.taskId = task.id

    ctx.session.taskExecuteAt = dayjs(task.execute_at).format()

    return await dateEditKeyboard(ctx)
}

/** Keyboard */
async function dateEditKeyboard(ctx) {
    const auth = await getAuth(ctx)

    const executeAt = dayjs(ctx.session.taskExecuteAt)
        .utcOffset(auth.utc)

    const formattedDate = firstCapital(
        executeAt.format('dd, D MMMM, YYYY года, HH:mm (Z)')
    )

    const yesterday = executeAt.subtract(1, 'day').format('dd, DD MMM')
    const tomorrow = executeAt.add(1, 'day').format('dd, DD MMM')

    let times = await auth.getItemValue(
        SETTING_TYPE.TIMES,
        [['10:00', '14:00', '18:00']]
    )

    if (typeof times === 'string') {
        times = JSON.parse(times)
    }

    times = times.map(elem =>
        elem.map(_elem =>
            Markup.button.callback(
                _elem,
                `task-edit-time-${_elem}`
            )
        )
    )

    let expiredText = ''

    if (dayjs().diff(executeAt) >= 0) {
        expiredText = '❗️Время раньше текущей даты, уведомления не будет.\n\n'
    }

    return [
        `Перенос задачи на <b>${formattedDate}</b>.\n\n` +
        expiredText +
        '<i>Измените дату выполнения задачи</i>.',
        {
            ...Markup.inlineKeyboard([
                [
                    Markup.button.callback(
                        `« ${firstCapital(yesterday)}`,
                        'task-edit-date-sub-day'
                    ),
                    Markup.button.callback(
                        `${firstCapital(tomorrow)} »`,
                        'task-edit-date-add-day'
                    ),
                ],
                [
                    Markup.button.callback(
                        '« 1 мес.',
                        'task-edit-date-sub-month'
                    ),
                    Markup.button.callback(
                        '« 1 нед.',
                        'task-edit-date-sub-week'
                    ),
                    Markup.button.callback(
                        '1 нед. »',
                        'task-edit-date-add-week'
                    ),
                    Markup.button.callback(
                        '1 мес. »',
                        'task-edit-date-add-month'
                    ),
                ],
                ...times,
                [
                    Markup.button.callback(
                        '« Назад',
                        `task-model-${ctx.session.taskId}`
                    ),
                    Markup.button.callback(
                        'Выбрать',
                        'task-edit-execute-select'
                    ),
                ],
            ]),
            parse_mode: 'HTML',
        }
    ]
}

/** Actions */
userScene.action(/task-edit-date-(sub|add)-(day|week|month)/, async ctx => {
    const operation = ctx.match[1]
    const dayRange = ctx.match[2]

    const auth = await getAuth(ctx)

    let executeAt = dayjs(ctx.session.taskExecuteAt)

    if (dayRange === 'day') {
        executeAt = executeAt.add(operation === 'add' ? 1 : -1, 'day')
    }
    else if (dayRange === 'week') {
        executeAt = executeAt.add(operation === 'add' ? 1 : -1, 'week')
    }
    else if (dayRange === 'month') {
        executeAt = executeAt.add(operation === 'add' ? 1 : -1, 'month')
    }

    ctx.session.taskExecuteAt = executeAt.utcOffset(auth.utc).format()

    return ctx.editMessageText(...await dateEditKeyboard(ctx))
})

userScene.action(/task-edit-time-(\d{1,2}[\:]\d{2})/, async ctx => {
    await ctx.answerCbQuery()

    const auth = await getAuth(ctx)

    let executeAt = dayjs(ctx.session.taskExecuteAt).utcOffset(auth.utc)

    const [hour, minute] = ctx.match[1].split(':')

    if (executeAt.hour() == hour && executeAt.minute() == minute) return

    executeAt = executeAt.hour(hour).minute(minute)

    ctx.session.taskExecuteAt = executeAt.format()

    return ctx.editMessageText(...await dateEditKeyboard(ctx))
})

userScene.action('task-edit-execute-select', async ctx => {
    const model = await Task.findByPk(ctx.session.taskId)

    if (!model) {
        return ctx.editMessageText(
            'Сообщение уже устарело.\n' +
            'Отправьте /newtask для создания новой задачи.'
        )
    }

    const executeAt = dayjs(ctx.session.taskExecuteAt)

    await model.update({
        is_notified: 0,
        is_completed: 0,
        execute_at: executeAt.format(),
    })

    await scheduleTask(ctx, model)

    onInput(ctx)

    delete ctx.session.taskId
    delete ctx.session.taskExecuteAt

    return ctx.editMessageText(
        'Задача успешно перенесена.',
        Markup.inlineKeyboard([
            [Markup.button.callback('« К задаче', `task-model-${model.id}`)],
        ])
    )
})

/** Messages */

/** Input date and time validation */
const dateRegex = {
    // HH:mm
    time: /\d{1,2}[\:]\d{2}.*/,
    // DD.MM
    dayMonth: /\d{1,2}[\.]\d{1,2}/,
    // DD.MM.YYYY
    fullDate: /\d{1,2}[\.]\d{1,2}[\.]\d{4}/,
    // HH:mm DD.MM
    timeDayMonth: /\d{1,2}[\:]\d{2}\s\d{1,2}[\.]\d{1,2}.*/,
    // HH:mm DD.MM.YYYY
    full: /\d{1,2}[\:]\d{2}\s\d{1,2}[\.]\d{1,2}[\.]\d{4}/,
}

export async function dateInput(ctx) {
    let value = ctx.message.text

    if (!value) {
        return ctx.reply('Дата не может быть пустой.')
    }

    const auth = await getAuth(ctx)

    value = value.replace(/\r?\n/g, '')

    let executeAt = dayjs(ctx.session.taskExecuteAt).utcOffset(auth.utc)
    const parts = value.split((/[\.\:\s]/))

    if (dateRegex.fullDate.test(value) || dateRegex.full.test(value)) {
        executeAt = executeAt.year(parts.pop())
    }
    if (dateRegex.dayMonth.test(value) || dateRegex.timeDayMonth.test(value)) {
        executeAt = executeAt.month(+parts.pop() - 1).date(parts.pop())
    }
    if (dateRegex.time.test(value)) {
        executeAt = executeAt.minute(parts.pop()).hour(parts.pop())
    }

    if (parts.length !== 0) {
        return ctx.reply('Дата введена не верно, проверьте формат.')
    }
    if (executeAt.year() <= 1970) {
        return ctx.reply('Ну и какой смысл создавать задачу за этот год 😏')
    }
    if (executeAt.diff(dayjs(), 'years') >= 10) {
        const maxYear = dayjs().add(10, 'years').format('YYYY')

        return ctx.reply(`Нельзя создать задачу дальше ${maxYear} года.`)
    }

    ctx.session.taskExecuteAt = executeAt.format()

    return ctx.reply(...await dateEditKeyboard(ctx))
}
