/**
 * Task execute edit
 */
import { Markup } from 'telegraf'

import { userScene } from '#scene'
import { backToModels } from '#utils/helpers.js'
import { Task } from '#models/task.js'
import { dateEditDialog } from './date.js'

/** Actions */
userScene.action(/task-edit-execute-(\w{10,})/, async ctx => {
    await ctx.answerCbQuery()

    const model = await Task.findByPk(ctx.match[1])

    if (!model) {
        return backToModels(ctx, 'Задача не найдена.', 'tasks')
    }

    if (model.is_completed) {
        return ctx.editMessageText(
            'Задача уже выполнена, ее нельзя перенести.',
            Markup.inlineKeyboard([
                [Markup.button.callback('« Назад', `task-model-${model.id}`)]
            ])
        )
    }

    return ctx.editMessageText(...await dateEditDialog(ctx))
})