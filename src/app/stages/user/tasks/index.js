/**
 * User's tasks
 */
import { Op } from 'sequelize'
import { Markup } from 'telegraf'

import { userScene } from '#scene'
import { removeTags } from '#utils/entities.js'
import { Task } from '#models/task.js'
import { getAuth } from '#controllers/userController.js'
import { taskEditKeyboard } from './edit/index.js'
import { newTaskDialog } from '#app/stages/user/newtask/index.js'

/** Keyboard */
export async function tasksKeyboard(ctx) {
    const auth = await getAuth(ctx)

    const ROWS_PER_PAGE = 5

    let page = 0
    let offset = 0

    if (ctx.match && /(-page-|back-to-)/.test(ctx.match[0])) {
        if (ctx.match[1]) {
            page = +ctx.match[1]
        }
        else if (ctx.session.tasksPage) {
            page = +ctx.session.tasksPage
        }

        offset = page * ROWS_PER_PAGE
    }

    let filterText = '⛊ Мои задачи'
    let search = {
        author_id: auth.id,
        target_id: auth.id,
    }

    switch (ctx.session.taskFilter) {
        case 1:
            filterText = '⛉ Для меня'
            search = {
                author_id: { [Op.not]: auth.id },
                target_id: auth.id,
                is_notified: 1,
            }
            break
        case 2:
            filterText = '⛉ От меня'
            search = {
                author_id: auth.id,
                target_id: { [Op.not]: auth.id },
            }
            break
    }

    let models = (await Task.findAll({
        where: { ...search },
        order: [['execute_at', 'desc']],
        offset: offset,
        limit: ROWS_PER_PAGE,
    }))

    for (let l = 0; l < models.length; l++) {
        const text = await textForButton(models[l])

        models[l] = [Markup.button.callback(text, `task-model-${models[l].id}`)]
    }

    const modelsCount = await Task.count({ where: search })

    return [
        'Задачи ' +
        `(${Math.min(offset + ROWS_PER_PAGE, modelsCount)}/${modelsCount})`,
        Markup.inlineKeyboard([
            ...models,
            [
                Markup.button.callback(
                    '«',
                    `tasks-page-${page - 1}`,
                    page === 0
                ),
                Markup.button.callback(
                    '»',
                    `tasks-page-${page + 1}`,
                    ROWS_PER_PAGE >= modelsCount - offset
                ),
            ],
            [
                Markup.button.callback(filterText, 'tasks-filter'),
                // Markup.button.callback('Создать', 'task-create'),
            ],
        ])
    ]
}

/**
 * Slice text to shorten it and clear it from unicode
 */
async function textForButton(task) {
    let mark = '⧗ '

    if (task.is_completed) mark = '✓ '
    else if (task.is_notified) mark = '• '

    let result = removeTags(task.text)
    result = result.replace(/\r?\n|\r/g, ' ')
    result = result.replace(/([\u2700-\u27BF]|[\uE000-\uF8FF]|\uD83C[\uDC00-\uDFFF]|\uD83D[\uDC00-\uDFFF]|[\u2011-\u26FF]|\uD83E[\uDD10-\uDDFF])/g, '')

    if (result.length === 0) {
        const file = await task.getFile()

        result = `[${file?.type_id || 'none'}]`
    }
    else if (result.length > 30) {
        result = result.slice(0, 30).trim() + '...'
    }

    return mark + result
}

/** Actions */
userScene.action(/tasks-page-(\d+)/, async ctx => {
    ctx.session.tasksPage = +ctx.match[1]

    return ctx.editMessageText(...await tasksKeyboard(ctx))
})

userScene.action(/task-model-(\w{10,})/, async ctx => {
    ctx.session.taskId = ctx.match[1]

    if (! await Task.findByPk(ctx.session.taskId)) {
        return ctx.editMessageText(...await tasksKeyboard(ctx))
    }

    return ctx.editMessageText(...await taskEditKeyboard(ctx))
})

userScene.action('tasks-filter', async ctx => {
    let _filter = ctx.session.taskFilter || 0

    _filter = _filter === 2 ? 0 : _filter + 1

    ctx.session.taskFilter = _filter

    await ctx.editMessageText(...await tasksKeyboard(ctx))
})

userScene.action('task-create', async ctx => {
    await ctx.answerCbQuery()

    await ctx.reply(...await newTaskDialog(ctx))
})

userScene.action('back-to-tasks', async ctx => {
    return ctx.editMessageText(...await tasksKeyboard(ctx))
})
