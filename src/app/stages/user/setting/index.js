/**
 * Settings
 */
import { Markup } from 'telegraf'
import { userScene } from '#scene'
import { utcSettingsKeyboard } from './utc.js'
import { tasksSettingsKeyboard } from './tasks/index.js'

/** Keyboard */
export async function settingsKeyboard(ctx) {
    return [
        'Настройки',
        Markup.inlineKeyboard([
            [ Markup.button.callback('Часовой пояс', 'settings-utc') ],
            [ Markup.button.callback('Задачи', 'settings-tasks') ],
        ])
    ]
}

/** Actions */
userScene.action('settings-utc', async ctx => {
    await ctx.answerCbQuery()

    return ctx.reply(...await utcSettingsKeyboard(ctx))
})

userScene.action('settings-tasks', async ctx => {
    return ctx.editMessageText(...await tasksSettingsKeyboard(ctx))
})

userScene.action('back-to-settings', async ctx => {
    return ctx.editMessageText(...await settingsKeyboard(ctx))
})
