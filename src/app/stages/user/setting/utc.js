/**
 * Utc settings
 */
import { fn, col, Op } from 'sequelize'

import { onInput } from '#scene'
import { User } from '#models/user.js'
import { getAuth } from '#controllers/userController.js'

/** Keyboard */
export async function utcSettingsKeyboard(ctx) {
    onInput(ctx, utcValue)

    let utcExamples = await User.findAll({
        attributes: [ 'utc' ],
        where: { utc: { [Op.not]: null } },
        group: 'utc',
        order: [[ fn('count', col('*')), 'desc' ]],
        limit: 5,
    })

    utcExamples = utcExamples.map(elem => elem.utc)
    utcExamples.push('+02:00', '+03:00', '+04:00', '+07:00', '+12:00')

    utcExamples = Array.from(new Set(utcExamples))
        .slice(0, 5)
        .sort()
        .map(elem => `<code>${elem}</code>`)
        .join(', ')

    const rows = [
        'Введите значение своего <b>часового пояса</b>.',
        `\n\n<i>Например:</i> ${utcExamples}.`,
    ]

    if (! ctx.match) {
        rows.splice(
            1, 0,
            '\n<i>Это необходимо для ' +
            'корректного управления задачами.</i>'
        )
    }

    return [
        rows.join(''),
        { parse_mode: 'HTML' },
    ]
}

/** Messages */
export async function utcValue(ctx) {
    let value = ctx.message.text

    if (! value) {
        return ctx.reply('Часовой пояс не может быть пустой.')
    }

    value = value.replace(/\r?\n/g, '')

    if (! /^[+-]{1}\d{2}:\d{2}/.test(value)) {
        return ctx.reply('Не верный формат часового пояса.')
    }

    const auth = await getAuth(ctx)

    await auth.update({ utc: value })

    onInput(ctx)

    return ctx.reply(
        'Часовой пояс установлен.\n' +
        'Введите /help для отображения списка команд.', {}
    )
}
