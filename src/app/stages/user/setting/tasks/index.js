/**
 * Poll settings
 */
import { Markup } from 'telegraf'

import { userScene } from '#scene'
import { onlyTextSettingsKeyboard } from './onlyText.js'

import './times.js'

/** Keyboard */
export async function tasksSettingsKeyboard(ctx) {
    return [
        'Настройки задач',
        Markup.inlineKeyboard([
            [ Markup.button.callback('Доп. информация', 'setting-tasks-only-text') ],
            [ Markup.button.callback('Кнопки времени', 'setting-tasks-time') ],
            [ Markup.button.callback('« Назад', 'back-to-settings') ],
        ])
    ]
}

/** Actions */
userScene.action('setting-tasks-only-text', async ctx => {
    return ctx.editMessageText(...await onlyTextSettingsKeyboard(ctx))
})

userScene.action('back-to-tasks-settings', async ctx => {
    return ctx.editMessageText(...await tasksSettingsKeyboard(ctx))
})
