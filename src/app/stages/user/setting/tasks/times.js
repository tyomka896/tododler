/**
 * Task times settings
 */
import { Markup } from 'telegraf'

import { userScene, onInput } from '#scene'
import { SETTING_TYPE } from '#models/user_setting_type.js'
import { getAuth } from '#controllers/userController.js'
import { hiddenTextLink } from '#utils/helpers.js'

/** Actions */
userScene.action('setting-tasks-time', async ctx => {
    await ctx.answerCbQuery()

    onInput(ctx, timesValues)

    return ctx.reply(
        'Введите диапазон значений со знаком / в качестве разделителя.\n\n' +
        '<i>Например:</i>' +
        hiddenTextLink('doc.tyomka896.ru/tododler/common/setting-tasks-times.png'),
        { parse_mode: 'HTML' }
    )
})

/** Messages */
export async function timesValues(ctx) {
    const auth = await getAuth(ctx)

    let values = ctx.message.text

    if (! values) {
        await auth.setItem(SETTING_TYPE.TIMES, [['10:00', '14:00', '18:00']])

        onInput(ctx)

        return ctx.reply(
            'Значения времен выставлены по умолчанию.',
            Markup.inlineKeyboard([
                [ Markup.button.callback('« Назад', 'back-to-settings') ],
            ])
        )
    }

    values = values.split(/\r?\n/g)

    if (values.length > 2) {
        return ctx.reply('Допускается не больше двух строк.')
    }

    for (let l = 0; l < values.length; l++) {
        const _mapped = values[l].split('/')

        if (_mapped.length > 4) {
            return ctx.reply(
                `В ${l + 1}-й строке превышено количество времен.\n` +
                'Допускается не более четырех значений на строку.'
            )
        }

        for (let p = 0; p < _mapped.length; p++) {
            if (! /\d{1,2}[\:\s]\d{2}/.test(_mapped[p])) {
                return ctx.reply(
                    `Не все значения в ${l + 1}-й строке верны.\n` +
                    'Допускается только значения времени.'
                )
            }
        }

        values[l] = _mapped
    }

    await auth.setItem(SETTING_TYPE.TIMES, values)

    onInput(ctx)

    return ctx.reply(
        'Значения времен успешно изменены.',
        Markup.inlineKeyboard([
            [ Markup.button.callback('« Назад', 'back-to-tasks-settings') ],
        ])
    )
}
