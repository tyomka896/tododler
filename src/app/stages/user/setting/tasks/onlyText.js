/**
 * Task times settings
 */
import { Markup } from 'telegraf'

import { userScene } from '#scene'
import { SETTING_TYPE } from '#models/user_setting_type.js'
import { getAuth } from '#controllers/userController.js'

/** Keyboard */
export async function onlyTextSettingsKeyboard(ctx) {
    const auth = await getAuth(ctx)

    const onlyTextItem = await auth.getItemValue(SETTING_TYPE.ONLY_TEXT, false)

    const buttonText = +onlyTextItem ? 'Включить' : 'Отключить'

    return [
        'Ввод дополнительной информации ' +
        (+onlyTextItem ? '<b>выключен</b>' : '<b>включен</b>') +
        ' при создании новой задачи.',
        {
            ...Markup.inlineKeyboard([
                [
                    Markup.button.callback(
                        buttonText,
                        'setting-tasks-toggle-only-text'
                    )
                ],
                [ Markup.button.callback('« Назад', 'back-to-tasks-settings') ],
            ]),
            parse_mode: 'HTML',
        }
    ]
}

/** Actions */
userScene.action('setting-tasks-toggle-only-text', async ctx => {
    const auth = await getAuth(ctx)

    const onlyTextItem = await auth.getItemValue(SETTING_TYPE.ONLY_TEXT, false)

    await auth.setItem(SETTING_TYPE.ONLY_TEXT, ! +onlyTextItem)

    return ctx.editMessageText(...await onlyTextSettingsKeyboard(ctx))
})
