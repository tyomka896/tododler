/**
 * Main stage
 */
import { userScene } from '#scene'
import help from '#app/commands/help.js'
import { getAuth } from '#controllers/userController.js'
import { addFndDialog } from './addfnd/index.js'
import { friendsKeyboard } from './friends/index.js'
import { friendsTaskKeyboard } from './newfndtask/index.js'
import { newTaskDialog } from './newtask/index.js'
import { settingsKeyboard } from './setting/index.js'
import { statusKeyboard } from './status/index.js'
import { utcSettingsKeyboard } from './setting/utc.js'
import { tasksKeyboard } from './tasks/index.js'

/** Actions */
userScene.enter(async ctx => {
    if (!(await getAuth(ctx)).utc) {
        ctx.scene.state.act = 'utc'
    }

    switch (ctx.scene.state.act) {
        case 'addfnd': return ctx.reply(...await addFndDialog(ctx))
        case 'friends': return ctx.reply(...await friendsKeyboard(ctx))
        case 'newfndtask': return ctx.reply(...await friendsTaskKeyboard(ctx))
        case 'newtask': return ctx.reply(...await newTaskDialog(ctx))
        case 'settings': return ctx.reply(...await settingsKeyboard(ctx))
        case 'status': return ctx.reply(...await statusKeyboard(ctx))
        case 'utc': return ctx.reply(...await utcSettingsKeyboard(ctx))
        case 'tasks': return ctx.reply(...await tasksKeyboard(ctx))
        default: return help(ctx)
    }
})

export { userScene }
