/**
 * Starting point of the bot
 */
import './config/env.js'
import { Telegraf } from 'telegraf'
import { Bootstrap } from './app/bootstrap.js'

/** Creating */
const bot = new Telegraf(env('BOT_TOKEN'))

/** Bootstrapping and launching bot */
await (await Bootstrap(bot)).launch()
